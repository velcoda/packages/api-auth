#!/bin/bash

# Configure git
echo "Identify as $GITLAB_USER_NAME <$GITLAB_USER_EMAIL>"
git config user.email "$GITLAB_USER_EMAIL"
git config user.name "$GITLAB_USER_NAME"

if [[ -z "$PUSH_TOKEN" ]]; then
    echo "You need create Project Access Token and set it value as pipeline variable PUSH_TOKEN"
    exit 1
fi

git remote remove origin
git remote add origin "https://oauth2:${PUSH_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git"

# Get highest tag number
OLD_VERSION=`git describe --abbrev=0 --tags 2>/dev/null`

if [ "$OLD_VERSION" ]; then
    # Get number parts and increase last one by 1
    MAJOR=$(echo "$OLD_VERSION" | cut -d"." -f1)
    MINOR=$(echo "$OLD_VERSION" | cut -d"." -f2)
    PATCH=$(echo "$OLD_VERSION" | cut -d"." -f3)
    MAJOR=`echo $MAJOR | sed 's/v//'`

    # Check for #major or #minor in commit message and increment the relevant version number
    UPDATE_MAJOR=`git log --format=%B -n 1 HEAD | grep '#major'`
    UPDATE_MINOR=`git log --format=%B -n 1 HEAD | grep '#minor'`

    if [ "$UPDATE_MAJOR" ]; then
        echo "Update major version"
        MAJOR=$((MAJOR + 1))
        MINOR=0
        PATCH=0
    elif [ "$UPDATE_MINOR" ]; then
        echo "Update minor version"
        MINOR=$((MINOR + 1))
        PATCH=0
    else
        echo "Update patch version"
        PATCH=$((PATCH + 1))
    fi

    # Create new tag
    NEW_VERSION="v$MAJOR.$MINOR.$PATCH"

    echo "Updating version $OLD_VERSION to $NEW_VERSION"
else
    NEW_VERSION="v1.0.0"
    echo "Creating version $NEW_VERSION"
fi

# Get current hash and see if it already has a tag
GIT_COMMIT=`git rev-parse HEAD`
NEEDS_TAG=`git describe --contains $GIT_COMMIT 2>/dev/null`

if [ -z "$NEEDS_TAG" ]; then
    echo "Tagged commit $CI_COMMIT_SHORT_SHA with $NEW_VERSION"
    git tag -a $NEW_VERSION $CI_COMMIT_SHORT_SHA -m "Bump version $NEW_VERSION"
    git push -o ci.skip origin $NEW_VERSION

    # Export version to file for use in next stages
    echo $NEW_VERSION >> VERSION
else
    echo "Already a tag $NEW_VERSION on commit $CI_COMMIT_SHORT_SHA"
fi
