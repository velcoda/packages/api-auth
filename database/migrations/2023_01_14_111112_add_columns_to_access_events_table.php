<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAccessEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('api_key_access_events', function($table) {
            $table->string('api_key_name')->nullable()->after('api_key_id');
            $table->string('method')->nullable()->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('api_key_access_events', function($table) {
            $table->dropColumn('api_key_name');
            $table->dropColumn('method');
        });
    }
}
