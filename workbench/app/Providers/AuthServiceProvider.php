<?php

declare(strict_types=1);

namespace Workbench\App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Velcoda\ApiAuth\Guards\VelcodaAuthGuard;

class AuthServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->registerPolicies();

        Auth::extend('velcoda', function ($app, $name, array $config) {
            return new VelcodaAuthGuard($app);
        });
    }
}
