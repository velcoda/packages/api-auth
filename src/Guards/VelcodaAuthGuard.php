<?php

namespace Velcoda\ApiAuth\Guards;

use Firebase\JWT\JWT;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Velcoda\ApiAuth\Http\Services\JWT\JwtTokenService;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;
use Velcoda\Exceptions\Exceptions\NotImplementedException;

class VelcodaAuthGuard implements Guard
{
    private $jwt;
    private $request;
    private IdentityBase|null $identity = null;

    /**
     * @throws GuzzleException
     */
    public function __construct($app)
    {
        // allow JWTs to be valid 10 seconds before they were generated.
        // This can become an issue, when a freshly generated token gets used
        // right away in other systems, and there is a small time-difference on the servers
        JWT::$leeway = 10;
        $this->request = $app->request;
        $this->jwt = $this->request->bearerToken();
    }

    /**
     * @return bool
     *
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_UNAUTHORIZED
     * @throws GuzzleException
     */
    public function check()
    {
        if ($this->identity) {
            return true;
        }
        if (!$this->jwt) {
            return false;
        }
        $service = new JwtTokenService($this->jwt);
        $this->identity = $service->getIdentity();
        return true;
    }

    /**
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNAUTHORIZED
     */
    public function guest(): bool
    {
        return !$this->check();
    }

    /**
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNAUTHORIZED
     */
    public function user(): ?IdentityBase
    {
        if ($this->identity) {
            return $this->identity;
        }

        if (!$this->jwt) {
            return null;
        }

        $service = new JwtTokenService($this->jwt);
        $this->identity = $service->getIdentity();

        return $this->identity;
    }

    /**
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_UNAUTHORIZED
     * @throws GuzzleException
     */
    public function id()
    {
        if ($this->identity) {
            return $this->identity?->id;
        }

        if (!$this->jwt) {
            return null;
        }

        $service = new JwtTokenService($this->jwt);
        $this->identity = $service->getIdentity();

        return $this->identity?->id;
    }

    public function validate(array $credentials = [])
    {
        throw new NotImplementedException('validate()');
    }

    public function hasUser(): bool
    {
        return !!$this->identity;
    }

    public function setUser(Authenticatable|IdentityBase $user)
    {
        $this->identity = $user;
    }
}
