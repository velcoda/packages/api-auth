<?php

namespace Velcoda\ApiAuth;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Velcoda\ApiAuth\Console\Commands\ActivateApiKey;
use Velcoda\ApiAuth\Console\Commands\DeactivateApiKey;
use Velcoda\ApiAuth\Console\Commands\DeleteApiKey;
use Velcoda\ApiAuth\Console\Commands\GenerateApiKey;
use Velcoda\ApiAuth\Console\Commands\ListApiKeys;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Velcoda\ApiAuth\Models\ApiKey;

class RateLimitServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->configureRateLimiting();
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('velcoda', function (Request $request) {
            $limiters = [];
            $api_key_limiter = $this->apiKeyRateLimiter($request);
            $default_limiter = $this->defaultRateLimiter($request);
            if ($api_key_limiter) {
                $limiters[] = $api_key_limiter;
            } else {
                $limiters[] = $default_limiter;
            }
            return $limiters;
        });
        $limit = env('RATE_LIMIT_HEALTHZ_PER_MINUTE', 100);
        RateLimiter::for('velcoda-healthz', function (Request $request) use ($limit) {
            return Limit::perMinute($limit)->by($request->user()?->id ?: $request->ip());
        });
    }

    private function defaultRateLimiter(Request $request): Limit
    {
        $limit = env('RATE_LIMIT_API_PER_MINUTE', 60);
        return Limit::perMinute($limit)->by($request->user()?->id ?: $request->ip());
    }

    private function apiKeyRateLimiter(Request $request): ?Limit
    {
        $token = $request->header(AuthorizeApiKey::AUTH_HEADER);
        if (!$token) {
            return null;
        }
        $api_key = ApiKey::where('key', '=', $token)->first();
        if (!$api_key) {
            return null;
        }
        $api_key_scopes = $api_key->scopes();
        $scope_name = $this->findApiKeyScopeInMiddlewares($request->route()->middleware());
        $scope = $api_key_scopes->where('scope', '=', $scope_name)->first();
        if (!$scope) {
            return null;
        }
        $rate_limit = $scope->rate_limit;
        $key = $api_key->id . '-' . $scope->id;
        return Limit::perMinute($rate_limit)->by($key);
    }

    private function findApiKeyScopeInMiddlewares(array $middlewares) {
        $prefix = 'auth.api-key:';
        $middleware_name_len = strlen($prefix);
        foreach ($middlewares as $middleware) {
            if (str_starts_with($middleware, $prefix)) {
                return substr($middleware, $middleware_name_len);
            }
        }
        return null;
    }
}
