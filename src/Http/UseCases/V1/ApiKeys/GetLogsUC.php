<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys;

use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyAccessEvent;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\TransactionFlow\UseCases\Base;

class GetLogsUC extends Base
{
    public function run()
    {
        $limit = $this->query('limit');
        $search = $this->query('search');

        $apiKeyId = $this->parameter('key_id');
        $apiKey = ApiKey::find($apiKeyId);

        if (!$apiKey) {
            throw new HTTP_NOT_FOUND('Api Key not found');
        }

        $logs = ApiKeyAccessEvent::where('api_key_id', '=', $apiKeyId);

        if ($search) {
            $logs = $logs->where(function($query) use ($search){
                $query->where('url', 'LIKE', '%'.$search.'%');
                $query->where('ip_address', 'LIKE', '%'.$search.'%');
            });
        }
        $logs = $logs->paginate($limit);
        return \Velcoda\ApiAuth\Http\Serializers\ApiKeyAccessEvent::collection($logs);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
