<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys;

use Illuminate\Support\Facades\Auth;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\Services\Http\Velcoda\Customers;
use Velcoda\TransactionFlow\UseCases\Base;

class CreateUC extends Base
{
    public function run()
    {
        $name = $this->get('name');
        $token = $this->get('token');
        $key = ApiKey::create($name, $token);
        return new \Velcoda\ApiAuth\Http\Serializers\ApiKey($key);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
