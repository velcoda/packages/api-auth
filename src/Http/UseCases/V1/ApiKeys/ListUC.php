<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Http\Serializers\ApiKeyWithScopes;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\Services\Http\Velcoda\Customers;
use Velcoda\TransactionFlow\UseCases\Base;

class ListUC extends Base
{
    public function run()
    {
        if ($this->query('filter') && key_exists('scope', $this->query('filter'))) {
            $scope = $this->query('filter')['scope'];
            $key_ids = DB::table('api_keys')
                ->leftJoin('api_key_scopes', 'api_keys.id', '=', 'api_key_scopes.api_key_id')
                ->where('api_key_scopes.scope', '=', $scope)
                ->pluck('api_keys.id')->toArray();
            $keys = ApiKey::whereIn('id', $key_ids)->get();
        } else {
            $keys = ApiKey::all();
        }

        return ApiKeyWithScopes::collection($keys);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
