<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys;

use Illuminate\Support\Facades\Auth;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\TransactionFlow\UseCases\Base;

class UpdateUC extends Base
{
    public function run()
    {
        $id = $this->parameter('key_id');
        $key = ApiKey::find($id);

        if (!$key) {
            throw new HTTP_NOT_FOUND('Api Key not found');
        }

        $active = !!$this->get('active');
        $key->active = $active;
        $key->save();

        return new \Velcoda\ApiAuth\Http\Serializers\ApiKey($key);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
