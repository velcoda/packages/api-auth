<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes;

use Illuminate\Support\Facades\Auth;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyScope;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\Http\Velcoda\Customers;
use Velcoda\TransactionFlow\UseCases\Base;

class ListUC extends Base
{
    public function run()
    {
        $key = ApiKey::find($this->parameter('key_id'));

        if (!$key) {
            throw new HTTP_NOT_FOUND('Api Key not found');
        }

        return response()->json($key->scopes);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
