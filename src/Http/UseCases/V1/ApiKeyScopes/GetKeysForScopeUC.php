<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyScope;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\Http\Velcoda\Customers;
use Velcoda\TransactionFlow\UseCases\Base;

class GetKeysForScopeUC extends Base
{
    public function run()
    {
        $id = $this->parameter('key_id');
        $scope = $this->parameter('scope_id');

        $key = ApiKey::find($id);
        if (!$key) {
            throw new HTTP_NOT_FOUND();
        }

        $sc = ApiKeyScope::find($scope);
        if (!$sc) {
            throw new HTTP_NOT_FOUND();
        }
        $sc->delete();
        return new \Velcoda\ApiAuth\Http\Serializers\ApiKey($key);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
