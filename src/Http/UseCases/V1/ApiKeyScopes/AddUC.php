<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyScope;
use Velcoda\Exceptions\Exceptions\HTTP_CONFLICT;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\Http\Velcoda\Customers;
use Velcoda\TransactionFlow\UseCases\Base;

class AddUC extends Base
{
    public function run()
    {
        $id = $this->parameter('key_id');
        $scope = $this->get('scope');
        $key = ApiKey::find($id);
        if (!$key) {
            throw new HTTP_NOT_FOUND('Api Key not found');
        }

        $existing = ApiKeyScope::where('api_key_id', '=', $key->id)->where('scope', '=', $scope)->first();
        if ($existing) {
            throw new HTTP_CONFLICT('scope already existing');
        }
        $sc = new ApiKeyScope();
        $sc->id = Str::uuid()->toString();
        $sc->api_key_id = $key->id;
        $sc->scope = $scope;
        $sc->save();

        return new \Velcoda\ApiAuth\Http\Serializers\ApiKey($key);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
