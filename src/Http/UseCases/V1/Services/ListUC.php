<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\Services;

use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\TransactionFlow\UseCases\Base;

class ListUC extends Base
{
    public function run()
    {
        $services = [];
        foreach (Service::cases() as $case) {
            $services[] = [
                'name' => $case->value,
                'hostname' => env($case->name . '_HOST')
            ];
        }
        return response()->json($services);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
