<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\Services;

use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\TransactionFlow\UseCases\Base;

class ShowUC extends Base
{
    public function run()
    {
        $meta = [
            'velcoda_access_key' => env('VELCODA_ACCESS_KEY'),
            'service' => env('APP_NAME'),
            'url' => env('APP_URL'),
        ];
        return response()->json($meta);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
