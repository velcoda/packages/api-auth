<?php

namespace Velcoda\ApiAuth\Http\UseCases\V1\Services;

use Illuminate\Support\Facades\Route;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\ApiAuth\Helpers\Token;
use Velcoda\TransactionFlow\UseCases\Base;

class GetAllApiRoutesUC extends Base
{
    public function run()
    {
        $routes = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            foreach ($route->middleware() as $mw) {
                if (str_starts_with($mw, 'auth.api-key:')) {
                    $routes[] = [
                        'route' => $route->uri,
                        'methods' => $route->methods(),
                        'scope' => explode(':', $mw)[1]
                    ];
                }
            }
        }
        return response()->json($routes);
    }

    // Authorize the request
    // return true if the request should be processed
    // return false if the request should return in a 403
    protected function authorizationRules(): bool
    {
        return Auth::user()->isAdmin();
    }
}
