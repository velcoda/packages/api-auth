<?php

namespace Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeyScopes;

use Respect\Validation\Validator as v;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\TransactionFlow\RequestHandlers\Base;

class DeleteRH extends Base
{
    /**
     * @return void
     *
     * @throws HTTP_BAD_REQUEST
     */
    protected function validateUrlParam()
    {
        $key_id = $this->parameter('key_id');
        $scope_id = $this->parameter('scope_id');
        $valid_key = v::uuid()->validate($key_id);
        $valid_scope = v::uuid()->validate($scope_id);
        if (!$valid_key || !$valid_scope) {
            throw new HTTP_BAD_REQUEST('UUID format required for URL param');
        }
    }

    protected function validateBody(): array
    {
        $this->validated = $this->request->validate([
            //
        ]);
        return $this->validated;
    }
}
