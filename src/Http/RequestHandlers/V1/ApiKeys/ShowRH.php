<?php

namespace Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys;

use Respect\Validation\Validator as v;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\TransactionFlow\RequestHandlers\Base;

class ShowRH extends Base
{
    /**
     * @return void
     *
     * @throws HTTP_BAD_REQUEST
     */
    protected function validateUrlParam()
    {
        $id = $this->parameter('key_id');
        $valid_param = v::uuid()->validate($id);
        if (!$valid_param) {
            throw new HTTP_BAD_REQUEST('UUID format required for URL param');
        }
    }

    protected function validateBody(): array
    {
        $this->validated = $this->request->validate([
            //
        ]);
        return $this->validated;
    }
}
