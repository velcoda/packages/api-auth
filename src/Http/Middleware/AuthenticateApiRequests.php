<?php

namespace Velcoda\ApiAuth\Http\Middleware;

use Closure;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Contracts\Auth\Middleware\AuthenticatesRequests;
use Velcoda\ApiAuth\Guards\VelcodaAuthGuard;
use Velcoda\ApiAuth\Http\Services\JWT\JwtTokenService;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyAccessEvent;
use Illuminate\Http\Request;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

class AuthenticateApiRequests implements AuthenticatesRequests
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;
    protected $required_scopes;

    /**
     * Create a new middleware instance.
     *
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string[] ...$guards
     * @return mixed
     *
     * @throws HTTP_UNAUTHORIZED
     */
    public function handle($request, Closure $next, ...$required_scopes)
    {
        $this->required_scopes = $required_scopes;
        $this->authenticate($request);
        return $next($request);
    }

    /**
     * Determine if the user provided a valid Access Token
     *
     * @param \Illuminate\Http\Request $request
     * @param array $guards
     * @return void
     *
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     */
    protected function authenticate($request)
    {
        $api_key = $request->header('x-api-key');
        $bearer_token = $request->bearerToken();
        $token = $api_key ?: $bearer_token;
        if ($token) {
            $jwtService = new JwtTokenService($token);
            self::checkScopes($this->required_scopes, $jwtService->scopes());
        }
        if ($this->auth->guard('velcoda')->check()) {
            $this->auth->shouldUse('velcoda');
            return;
        }
        throw new HTTP_UNAUTHORIZED('Unauthenticated.');
    }

    // all scopes in the required array must be present
    private static function checkScopes(array $required, array $present)
    {
        if (in_array('*', $present)) {
            return;
        }

        foreach ($required as $required_scope) {
            if (!in_array($required_scope, $present)) {
                throw new HTTP_UNAUTHORIZED('Scopes do not allow this action.');
            }
        }
    }
}
