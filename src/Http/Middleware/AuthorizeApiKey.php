<?php

namespace Velcoda\ApiAuth\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyAccessEvent;
use Illuminate\Http\Request;
use Velcoda\ApiAuth\Models\ApiKeyScope;
use Velcoda\Exceptions\Exceptions\HTTP_FORBIDDEN;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

class AuthorizeApiKey
{
    const AUTH_HEADER = 'x-api-key';

    /**
     * Handle the incoming request
     *
     * @param Request $request
     * @param Closure $next
     * @param mixed ...$scopes
     * @return ResponseFactory|mixed|Response
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_INTERNAL_SERVER
     */
    public function handle(Request $request, Closure $next, ...$scopes)
    {
        $header = $request->header(self::AUTH_HEADER);
        if (!$header) {
            throw new HTTP_UNAUTHORIZED();
        }
        $apiKey = ApiKey::getByKey($header);

        if ($apiKey) {
            if (self::hasCorrectScope($scopes, $apiKey->scopes)) {
                $this->logAccessEvent($request, $apiKey, 'ok');
                return $next($request);
            } else {
                $this->logAccessEvent($request, $apiKey, json_encode([
                    'status' => 'DENIED',
                    'reason' => 'insufficient scopes',
                    'requested scopes' => implode(',', $scopes),
                ]));
                throw new HTTP_FORBIDDEN('Insufficient scopes!');
            }
        }
        $this->logAccessEvent($request, null, json_encode([
            'status' => 'DENIED',
            'reason' => 'api_key not found',
            'requested scopes' => implode(',', $scopes),
            'api-key' => $header,
            'host' => $request->host(),
        ]));
        throw new HTTP_UNAUTHORIZED();
    }

    private static function hasCorrectScope($required_scopes, $provided_api_key_scopes): bool {
        if (!is_array($required_scopes)) {
            $required_scopes = explode(',', $required_scopes);
        }
        foreach ($provided_api_key_scopes as $scope) {
            if ($scope instanceof ApiKeyScope) {
                if (in_array($scope->scope, $required_scopes)) {
                    return true;
                }
            } else {
                throw new HTTP_INTERNAL_SERVER('Scope of wrong type');
            }
        }
        return false;
    }

    /**
     * Log an API key access event
     *
     * @param Request $request
     * @param ApiKey|null $api_key_id
     * @param $context
     */
    protected function logAccessEvent(Request $request, ApiKey|null $api_key, $context): void
    {
        $event = new ApiKeyAccessEvent;
        $event->id = Str::uuid()->toString();
        $event->api_key_id = $api_key?->id;
        $event->api_key_name = $api_key?->name;
        $event->ip_address = $request->ip();
        $event->url        = $request->fullUrl();
        $event->method        = $request->method();
        $event->context     = $context;
        $event->save();
    }
}
