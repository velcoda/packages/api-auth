<?php

namespace Velcoda\ApiAuth\Http\Services\JWT;

use Aws\S3\S3Client;
use Aws\Sts\StsClient;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Strobotti\JWK\KeyFactory;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;
use UnexpectedValueException;

class JwtTokenService
{
    private string $jwt;
    private \stdClass $decoded_jwt;

    /**
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNAUTHORIZED
     */
    public function __construct(string $jwt)
    {
        $this->jwt = $jwt;

        $this->decoded_jwt = $this->decodeJWT($this->jwt);
    }

    public function getIdentity(): IdentityBase
    {
        return $this->castToIdentity($this->decoded_jwt->identity);
    }

    public function sub(): string {
        return $this->decoded_jwt->sub;
    }

    public function jwt_id(): string {
        return $this->decoded_jwt->jti;
    }

    public function audience(): string {
        return $this->decoded_jwt->aud;
    }

    public function iat(): int {
        return $this->decoded_jwt->iat;
    }

    public function nbf(): int {
        return $this->decoded_jwt->nbf;
    }

    public function exp(): int {
        return $this->decoded_jwt->exp;
    }

    public function scopes(): array {
        return $this->decoded_jwt->scopes;
    }

    /**
     * @param $jwt
     *
     * @return \stdClass
     *
     * @throws GuzzleException
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_UNAUTHORIZED
     */
    private function decodeJWT($jwt): \stdClass
    {
        if (!$jwt) {
            throw new HTTP_UNAUTHORIZED();
        }
        CertificateService::loadKeys();
        $jwk = json_decode(Cache::get('jwt-keys'), true);
        try {
            return JWT::decode($jwt, JWK::parseKeySet($jwk)[0]);
        } catch (ExpiredException $e) {
            throw new HTTP_UNAUTHORIZED('Access Token Expired', 401, $e);
        } catch (SignatureInvalidException $e) {
            throw new HTTP_UNAUTHORIZED('Invalid Access Token Signature', 401, $e);
        } catch (BeforeValidException $e) {
            throw new HTTP_UNAUTHORIZED('Access Token not yet valid', 401, $e);
        } catch (UnexpectedValueException $e) {
            throw new HTTP_UNAUTHORIZED('Access Token malformed', 401, $e);
        } catch (\Exception $e) {
            throw new HTTP_INTERNAL_SERVER($e->getMessage(), 500, $e);
        }
    }

    private function castToIdentity($data): IdentityBase
    {
        $id = new IdentityBase();
        $id->id = $data->id;
        $id->customer_id = $data->customer_id;
        $id->email = $data->email;
        $id->title_before = $data->title_before;
        $id->first_name = $data->first_name;
        $id->middle_names = $data->middle_names;
        $id->last_name = $data->last_name;
        $id->title_after = $data->title_after;
        $id->gender = $data->gender;
        if (property_exists($data, 'email_verified_at')) {
            $id->email_verified_at = $data->email_verified_at;
        }
        $id->is_user = property_exists($data, 'is_user');
        $id->is_admin = property_exists($data, 'is_admin');
        $id->is_devops_admin = property_exists($data, 'is_devops_admin');
        $id->is_partner = property_exists($data, 'is_partner');
        $id->is_recording_studio = property_exists($data, 'is_recording_studio');
        return $id;
    }
}
