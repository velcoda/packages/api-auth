<?php

namespace Velcoda\ApiAuth\Http\Services\JWT;

use Aws\S3\S3Client;
use Aws\Sts\StsClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Strobotti\JWK\KeyFactory;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;

class CertificateService
{
    /**
     * @throws \RuntimeException
     */
    public static function loadKeys(): bool
    {
        $options = [
            'use' => 'sig',
            'alg' => 'RS256',
            'kty' => 'RSA',
        ];

        $publicKey = self::getPublicKey();

        $keyFactory = new KeyFactory();
        $key = $keyFactory->createFromPem($publicKey, $options);

        $data = [
            'keys' => [
                $key->jsonSerialize()
            ]
        ];

        Cache::put('jwt-keys', json_encode($data));

        return Cache::has('jwt-keys');
    }

    /**
     * @throws \RuntimeException
     */
    private static function getPublicKey(): string
    {
        $publicKey = env('JWT_PUBLIC_KEY');

        if (!$publicKey) {
            throw new \RuntimeException('Set valid JWT public key in environment JWT_PUBLIC_KEY variable.');
        }

        $publicKey = str_replace("----- ", "-----\n", $publicKey);
        $publicKey = str_replace(" -----", "\n-----", $publicKey);

        return $publicKey;
    }
}
