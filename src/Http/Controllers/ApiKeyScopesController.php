<?php

namespace Velcoda\ApiAuth\Http\Controllers;

use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeyScopes\AddRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeyScopes\DeleteRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeyScopes\GetKeysForScopeRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeyScopes\ListRH;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes\AddUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes\DeleteUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes\GetKeysForScopeUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes\ListUC;
use Velcoda\TransactionFlow\TransactionFlow;

class ApiKeyScopesController extends Controller
{
    
    public function list()
    {
        return TransactionFlow::new()
            ->request_handler(ListRH::class)
            ->use_case(ListUC::class)
            ->run();
    }

    public function add()
    {
        return TransactionFlow::new()
            ->request_handler(AddRH::class)
            ->use_case(AddUC::class)
            ->run();
    }

    public function remove()
    {
        return TransactionFlow::new()
            ->request_handler(DeleteRH::class)
            ->use_case(DeleteUC::class)
            ->run();
    }
    
    public function getKeysForScope()
    {
        return TransactionFlow::new()
            ->request_handler(GetKeysForScopeRH::class)
            ->use_case(GetKeysForScopeUC::class)
            ->run();
    }
}
