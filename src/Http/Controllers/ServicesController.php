<?php

namespace Velcoda\ApiAuth\Http\Controllers;

use Velcoda\ApiAuth\Http\RequestHandlers\V1\Services\GetAllApiRoutesRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\Services\ListRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\Services\ShowRH;
use Velcoda\ApiAuth\Http\UseCases\V1\Services\GetAllApiRoutesUC;
use Velcoda\ApiAuth\Http\UseCases\V1\Services\ListUC;
use Velcoda\ApiAuth\Http\UseCases\V1\Services\ShowUC;
use Velcoda\TransactionFlow\TransactionFlow;

class ServicesController extends Controller
{
    public function getAllServices()
    {
        return TransactionFlow::new()
            ->request_handler(ListRH::class)
            ->use_case(ListUC::class)
            ->run();
    }

    public function getAllRoutes()
    {
        return TransactionFlow::new()
            ->request_handler(GetAllApiRoutesRH::class)
            ->use_case(GetAllApiRoutesUC::class)
            ->run();
    }

    public function show()
    {
        return TransactionFlow::new()
            ->request_handler(ShowRH::class)
            ->use_case(ShowUC::class)
            ->run();
    }
}
