<?php

namespace Velcoda\ApiAuth\Http\Controllers;

use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys\CreateRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys\DeleteRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys\GetLogsRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys\ListRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys\ShowRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeys\UpdateRH;
use Velcoda\ApiAuth\Http\RequestHandlers\V1\ApiKeyScopes\AddRH;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys\CreateUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys\DeleteUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys\GetLogsUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys\ListUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys\ShowUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeys\UpdateUC;
use Velcoda\ApiAuth\Http\UseCases\V1\ApiKeyScopes\AddUC;
use Velcoda\TransactionFlow\TransactionFlow;

class ApiKeysController extends Controller
{
    public function create()
    {
        return TransactionFlow::new()
            ->request_handler(CreateRH::class)
            ->use_case(CreateUC::class)
            ->run();
    }

    public function list()
    {
        return TransactionFlow::new()
            ->request_handler(ListRH::class)
            ->use_case(ListUC::class)
            ->run();
    }

    public function show()
    {
        return TransactionFlow::new()
            ->request_handler(ShowRH::class)
            ->use_case(ShowUC::class)
            ->run();
    }

    public function update()
    {
        return TransactionFlow::new()
            ->request_handler(UpdateRH::class)
            ->use_case(UpdateUC::class)
            ->run();
    }

    public function delete()
    {
        return TransactionFlow::new()
            ->request_handler(DeleteRH::class)
            ->use_case(DeleteUC::class)
            ->run();
    }

    public function getLogs()
    {
        return TransactionFlow::new()
            ->request_handler(GetLogsRH::class)
            ->use_case(GetLogsUC::class)
            ->run();
    }
}
