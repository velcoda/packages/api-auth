<?php

namespace Velcoda\ApiAuth\Enums;

enum Service: string
{
    case IDENTITIES = 'identities';
    case CUSTOMERS = 'customers';
    case ADDRESS_BOOK = 'address-book';
    case CUSTOMER_SETTINGS = 'customer-settings';
    case USER_SETTINGS = 'user-settings';
    case AUDIOFILES = 'audiofiles';
    case ABSENCES = 'absences';
    case EXT_OUTLOOK = 'ext-outlook';
    case EXT_TELEMATICA = 'ext-telematica';
    case EXT_STARFACE = 'ext-starface';
    case EXT_BENEFIT_BUEROSERVICE = 'ext-benefit-bueroservice';
    case NOTIFICATION_GATEWAY = 'notification-gateway';
    case PARTNER_PORTAL = 'bff-partner-portal';
    case BFF_TELEBUTLER = 'bff-telebutler';
}
