<?php

namespace Velcoda\ApiAuth\Enums;

abstract class AllowedScopes {
    private const TELEBUTLER = 'telebutler';
    private const PARTNER = 'partner';
    private const OUTLOOK_ADDIN = 'outlook-addin';

    public static $telebutler = self::TELEBUTLER;
    public static $partner = self::PARTNER;
    public static $outlook_addin = self::OUTLOOK_ADDIN;
}
