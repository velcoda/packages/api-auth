<?php

namespace Velcoda\ApiAuth;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Velcoda\ApiAuth\Console\Commands\ActivateApiKey;
use Velcoda\ApiAuth\Console\Commands\CreateApiKeyWithScopes;
use Velcoda\ApiAuth\Console\Commands\DeactivateApiKey;
use Velcoda\ApiAuth\Console\Commands\DeleteApiKey;
use Velcoda\ApiAuth\Console\Commands\GenerateApiKey;
use Velcoda\ApiAuth\Console\Commands\ListApiKeys;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Velcoda\ApiAuth\Models\ApiKey;

class ApiKeyAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->registerMiddleware($router);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->registerRoutes();
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => '/' . env('APP_NAME') . '/v0/velcoda',
            'middleware' => 'api',
        ];
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->commands([
            ActivateApiKey::class,
            DeactivateApiKey::class,
            DeleteApiKey::class,
            GenerateApiKey::class,
            ListApiKeys::class,
            CreateApiKeyWithScopes::class,
        ]);
    }

    /**
     * Register middleware
     *
     * Support added for different Laravel versions
     *
     * @param Router $router
     */
    protected function registerMiddleware(Router $router)
    {
        $versionComparison = version_compare(app()->version(), '5.4.0');

        if ($versionComparison >= 0) {
            $router->aliasMiddleware('auth.api-key', AuthorizeApiKey::class);
        } else {
            $router->middleware('auth.api-key', AuthorizeApiKey::class);
        }
    }

    /**
     * Register migrations
     */
    protected function registerMigrations($migrationsDirectory)
    {
        $this->publishes([
            $migrationsDirectory => database_path('migrations')
        ], 'migrations');
    }
}
