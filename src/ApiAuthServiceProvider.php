<?php

namespace Velcoda\ApiAuth;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Velcoda\ApiAuth\Http\Middleware\AuthenticateApiRequests;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Velcoda\ApiAuth\Models\ApiKey;

class ApiAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->registerMiddleware($router);
        $this->mergeConfigFrom(__DIR__.'/../config/api_auth.php', 'filesystems.disks');
    }

    /**
     * Register middleware
     *
     * Support added for different Laravel versions
     *
     * @param Router $router
     */
    protected function registerMiddleware(Router $router)
    {
        $versionComparison = version_compare(app()->version(), '5.4.0');

        if ($versionComparison >= 0) {
            $router->aliasMiddleware('auth.api', AuthenticateApiRequests::class);
        } else {
            $router->middleware('auth.api', AuthenticateApiRequests::class);
        }
    }
}
