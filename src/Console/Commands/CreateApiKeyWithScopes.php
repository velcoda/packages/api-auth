<?php

namespace Velcoda\ApiAuth\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyScope;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

class CreateApiKeyWithScopes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vlc:apikey-configure
                            {service : Requesting service}
                            {--scopes= :Scopes this api key needs to have}
                            {--auth= : Authorization Bearer Token}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate an API key by name';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('service');
        $wanted_scopes = explode(',', $this->option('scopes'));
        $auth = $this->option('auth');

        if (!$auth) {
            throw new HTTP_UNAUTHORIZED('Authorization is required. Use --auth option.');
        }


        $meta_url = self::getHostname($name) . '/' . $name . '/v0/velcoda/meta';

        $key = ApiKey::where('name', 'service.' . $name)->first();
        if (!$key) {
            $client = new Client();
            $resp = $client->get($meta_url, ['headers' => ['Authorization' => 'Bearer ' . $auth]]);
            $body = json_decode($resp->getBody()->getContents());
            $key = ApiKey::create('service.' . $name, $body->velcoda_acces_key);
            dd($key);
        } else {
            $saved_scopes = ApiKeyScope::where('api_key_id', '=', $key->id)->pluck('scope')->toArray();
            foreach ($wanted_scopes as $scope) {
                if(!in_array($scope, $saved_scopes)) {
                    $s = new ApiKeyScope();
                    $s->id = Str::uuid();
                    $s->api_key_id = $key->id;
                    $s->scope = $scope;
                    $s->rate_limit = 100;
                    $s->save();
                    $this->info('Created scope: ' . $scope);
                }
            }
        }
    }

    private static function getHostname($service_name) {
        $env = env('APP_ENV');
        if (!$env) {
            throw new HTTP_INTERNAL_SERVER('APP_ENV not set!');
        }
        if ($env === 'production' || $env === 'staging') {
            return 'http://' . $service_name . '-service.' . $service_name . '.svc.cluster.local';
        }
        $service_name = str_replace('-', '_', $service_name);
        return env(strtoupper($service_name) . '_HOST');
    }
}
