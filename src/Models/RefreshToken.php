<?php

namespace Velcoda\ApiAuth\Models;


use Velcoda\Helpers\Model;

class RefreshToken extends Model
{
    public static function store($identity_id, $refresh_token) {
        self::forget($identity_id);
        $rt = new self();
        $rt->id = \Illuminate\Support\Str::uuid()->toString();
        $rt->identity_id = $identity_id;
        $rt->refresh_token = $refresh_token;
        $rt->save();
    }

    public static function forget($identity_id) {
        self::where('identity_id', '=', $identity_id)->delete();
    }

    public static function get($identity_id) {
        $rt = self::where('identity_id', '=', $identity_id)->first();
        if ($rt) {
            return $rt->refresh_token;
        }
        return null;
    }
}
