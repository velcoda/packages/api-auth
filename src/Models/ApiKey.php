<?php

namespace Velcoda\ApiAuth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Velcoda\ApiAuth\Enums\Service;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\Exceptions\Exceptions\HTTP_CONFLICT;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

class ApiKey extends Model
{
    use SoftDeletes;

    protected static $nameRegex = '/^[a-z0-9-]{1,255}$/';

    public $incrementing = false;
    protected $keyType = 'string';
    protected $table = 'api_keys';

    /**
     * Get the related ApiKeyAccessEvents records
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accessEvents()
    {
        return $this->hasMany(ApiKeyAccessEvent::class, 'api_key_id');
    }

    public static function create($name, $key = null, $active = true) {
        $data = explode('.', $name);
        if (count($data) !== 2) {
            throw new HTTP_BAD_REQUEST('Api Key must have one dot in name!');
        }
        $type = $data[0];
        $nameWithoutPrefix = $data[1];

        if ($type === 'service') {
            $isService = false;
            foreach (Service::cases() as $service) {
                if($nameWithoutPrefix === $service->value) {
                    $isService = true;
                }
            }
            if (!$isService) {
                throw new HTTP_INTERNAL_SERVER('Service not known!');
            }
        }


        $exists = ApiKey::where('name', '=', $name)->first();
        if ($exists) {
            throw new HTTP_CONFLICT('ApiKey with this name already exists');
        }
        $a = new self();
        $a->id = Str::uuid()->toString();
        $a->name = $name;
        $a->key = $key?: Str::random(64);
        $a->active = $active;
        $a->save();
        return new \Velcoda\ApiAuth\Http\Serializers\ApiKey($a);
    }


    /**
     * Get the related scopes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scopes()
    {
        return $this->hasMany(ApiKeyScope::class, 'api_key_id');
    }


    /**
     * Get ApiKey record by key value
     *
     * @param string $key
     * @return ApiKey|null
     */
    public static function getByKey($key): ?ApiKey
    {
        $key = self::where([
            'key'    => $key,
            'active' => 1
        ])->first();
        return $key ?: null;
    }

    /**
     * Check if key is valid
     *
     * @param string $key
     * @return bool
     */
    public static function isValidKey($key)
    {
        return self::getByKey($key) instanceof self;
    }

    /**
     * Check if name is valid format
     *
     * @param string $name
     * @return bool
     */
    public static function isValidName($name)
    {
        return (bool) preg_match(self::$nameRegex, $name);
    }

    /**
     * Check if a key already exists
     *
     * Includes soft deleted records
     *
     * @param string $key
     * @return bool
     */
    public static function keyExists($key)
    {
        return self::where('key', $key)->withTrashed()->first() instanceof self;
    }

    /**
     * Check if a name already exists
     *
     * Does not include soft deleted records
     *
     * @param string $name
     * @return bool
     */
    public static function nameExists($name)
    {
        return self::where('name', $name)->first() instanceof self;
    }


    protected $casts = [
        'active' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $dates = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];
}
