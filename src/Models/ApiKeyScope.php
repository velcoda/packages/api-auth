<?php

namespace Velcoda\ApiAuth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ApiKeyScope extends Model
{
    protected $table = 'api_key_scopes';
    public $incrementing = false;
    protected $keyType = 'string';
}
