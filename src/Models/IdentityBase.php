<?php

namespace Velcoda\ApiAuth\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class IdentityBase extends Authenticatable
{
    use SoftDeletes;

    const GENDER_FEMALE = 'f';
    const GENDER_MALE = 'm';
    const GENDER_COMPANY = 'c';

    public $incrementing = false;

    protected $keyType = 'string';

    public function full_name_polite() {
        $plugs = [];
        if ($this->title_before) {
            $plugs[] = $this->title_before;
        }

        $plugs[] = $this->first_name;

        if ($this->middle_names) {
            $plugs[] = $this->middle_names;
        }

        $plugs[] = $this->last_name;

        if ($this->title_after) {
            $plugs[] = $this->title_after;
        }
        return implode(' ', $plugs);
    }

    public function short_name_polite() {
        $plugs = [];
        if ($this->gender === 'm') {
            $plugs[] = 'Herr';
        }
        $plugs[] = $this->last_name;

        return implode(' ', $plugs);
    }

    public static function fromJsonObj($obj) {
        $identity = new self();
        $identity->id = $obj?->id;
        $identity->customer_id = $obj?->customer_id;
        $identity->title_before = $obj?->title_before;
        $identity->first_name = $obj?->first_name;
        $identity->middle_names = $obj?->middle_names;
        $identity->last_name = $obj?->last_name;
        $identity->title_after = $obj?->title_after;
        $identity->gender = $obj?->gender;
        $identity->email = $obj?->email;
        $identity->email_verified_at = $obj?->email_verified_at;
        if (property_exists($obj, 'is_user')) {
            $identity->is_user = (bool) $obj->is_user;
        }
        if (property_exists($obj, 'is_admin')) {
            $identity->is_admin = (bool) $obj->is_admin;
        }
        if (property_exists($obj, 'is_devops_admin')) {
            $identity->is_devops_admin = (bool) $obj->is_devops_admin;
        }
        if (property_exists($obj, 'is_partner')) {
            $identity->is_partner = (bool) $obj->is_partner;
        }
        if (property_exists($obj, 'is_recording_studio')) {
            $identity->is_recording_studio = (bool) $obj->is_recording_studio;
        }
        if (property_exists($obj, 'avatar_url')) {
            $identity->avatar_url = $obj?->avatar_url;
        }
        $identity->created_at = $obj?->created_at;
        $identity->updated_at = $obj?->updated_at;

        return $identity;
    }

    public function toArray(): array
    {
        $ret = [
            'id'                => $this->id,
            'customer_id'       => $this->customer_id,
            'title_before'      => $this->title_before,
            'first_name'        => $this->first_name,
            'middle_names'      => $this->middle_names,
            'last_name'         => $this->last_name,
            'title_after'       => $this->title_after,
            'gender'            => $this->gender,
            'email'             => $this->email,
            'email_verified_at' => $this->email_verified_at,
        ];

        if ($this->is_user) {
            $ret['is_user'] = $this->is_user;
        }
        if ($this->is_admin) {
            $ret['is_admin'] = $this->is_admin;
        }
        if ($this->is_devops_admin) {
            $ret['is_devops_admin'] = $this->is_devops_admin;
        }
        if ($this->is_partner) {
            $ret['is_partner'] = $this->is_partner;
        }
        if ($this->is_recording_studio) {
            $ret['is_recording_studio'] = $this->is_recording_studio;
        }
        if ($this->avatar_url) {
            $ret['avatar_url'] = $this->avatar_url;
        }

        $ret['created_at'] = $this->created_at;
        $ret['updated_at'] = $this->updated_at;
        if ($this->deleted_at) {
            $ret['deleted_at'] = $this->deleted_at;
        }
        return $ret;
    }

    public static function fromArray($arr): static
    {
        $identity = new self();
        $identity->id = $arr['id'];
        $identity->customer_id = $arr['customer_id'];
        $identity->title_before = $arr['title_before'];
        $identity->first_name = $arr['first_name'];
        $identity->middle_names = $arr['middle_names'];
        $identity->last_name = $arr['last_name'];
        $identity->title_after = $arr['title_after'];
        $identity->gender = $arr['gender'];
        $identity->email = $arr['email'];
        $identity->email_verified_at = $arr['email_verified_at'];
        if (key_exists('is_user', $arr)) {
            $identity->is_user = $arr['is_user'];
        }
        if (key_exists('is_admin', $arr)) {
            $identity->is_admin = $arr['is_admin'];
        }
        if (key_exists('is_devops_admin', $arr)) {
            $identity->is_devops_admin = $arr['is_devops_admin'];
        }
        if (key_exists('is_partner', $arr)) {
            $identity->is_partner = $arr['is_partner'];
        }
        if (key_exists('is_recording_studio', $arr)) {
            $identity->is_recording_studio = $arr['is_recording_studio'];
        }
        if (key_exists('avatar_url', $arr)) {
            $identity->avatar_url = $arr['avatar_url'];
        }
        $identity->created_at = $arr['created_at'];
        $identity->updated_at = $arr['updated_at'];
        if (key_exists('deleted_at', $arr)) {
            $identity->deleted_at = $arr['deleted_at'];
        }
        return $identity;
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'is_user'             => 'boolean',
        'is_admin'            => 'boolean',
        'is_devops_admin'     => 'boolean',
        'is_partner'          => 'boolean',
        'is_recording_studio' => 'boolean',
        'email_verified_at'   => 'timestamp',
        'created_at'          => 'timestamp',
        'updated_at'          => 'timestamp',
        'deleted_at'          => 'timestamp',
    ];

    public function isAdmin(): bool
    {
        return $this->is_admin ?? false;
    }

    public function isDevopsAdmin(): bool
    {
        return $this->is_devops_admin ?? false;
    }

    public function isPartner(): bool
    {
        return $this->is_partner ?? false;
    }

    public function isRecordingStudio(): bool
    {
        return $this->is_recording_studio ?? false;
    }
}
