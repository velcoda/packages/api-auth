<?php

namespace Velcoda\ApiAuth\Helpers;


use Illuminate\Support\Facades\Auth;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Services\DB\Velcoda\Customers;
use Velcoda\Services\DB\Velcoda\Identities;
use Velcoda\Services\Http\Telebutler\UserSettings;

trait CommonAuthorizationRules
{
    // check if a passed identity is the one of the logged in user.
    public function isMe($identity_id) {
        return Auth::id() === $identity_id;
    }

    /*
     * Checks if the logged-in user has the privileges of managing another user.
     * This is checked by the matching customer_id of both users, and by the acting users user-settings.
     * We load the settings of the acting (logged-in user) and check for the manage_users flat. It must be true
     */
    public function canManageCoworker(IdentityBase $coworker_identity) {
        if (Auth::guest()) {
            return false;
        }

        $settings = UserSettings::client();
        $settings->setApiKey();
        $settings = $settings->loadSetting(Auth::id())?->body()->data;

        return $coworker_identity->customer_id === Auth::user()->customer_id && $settings->manage_users;
    }

    // checks if the logged-in user is either admin or is the managing agent of the customer.
    // we check this by loading the customers details, and look if the logged-id users customer_id
    // matches the agent_customer_id of the users' customer we want to check for.
    public function isAdminOrAgentOfCustomerByIdentityId($identity_id): bool {
        // allow access to settings to all users by admin
        if (Auth::user()?->isAdmin()) {
            return true;
        }
        // partners can only access users they are agent for
        if (Auth::user()?->isPartner()) {
            $identity = Identities::getById($identity_id);
            if (!$identity) {
                return false;
            }
            $customer = Customers::getById($identity->customer_id);
            if (!$customer) {
                return false;
            }
            return $customer->agent_customer_id === Auth::user()->customer_id ||
                Auth::user()->customer_id === $customer->id;
        }
        return false;
    }

    // checks if the logged in user is either admin or is the managing agent of the customer.
    // we check this by loading the customers details, and look if the logged-id users customer_id
    // matches the agent_customer_id of the customer we want to check for.
    public function isAdminOrAgentOfCustomer($customer_id): bool {
        // allow access to settings to all users by admin
        if (Auth::user()?->isAdmin()) {
            return true;
        }
        // partners can only access users they are agent for
        if (Token::isPartner()) {
            $customer = Customers::getById($customer_id);
            if (!$customer) {
                return false;
            }
            return $customer->agent_customer_id === Auth::user()->customer_id ||
                Auth::user()->customer_id === $customer->id;
        }
        return false;
    }
}