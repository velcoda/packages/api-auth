<?php
namespace Velcoda\ApiAuth\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Cache;
use Velcoda\ApiAuth\Guards\VelcodaAuthGuard;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\ApiAuth\Models\RefreshToken;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;
use Velcoda\Services\Http\Velcoda\Identities;

class Token {
    /**
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_UNAUTHORIZED
     */
    public static function isValid($jwt): bool {
        return !!self::decode($jwt);
    }

    public static function decode($jwt) {
        // make AT valid even a few seconds before. happens when servers have time difference.
        JWT::$leeway = 10; // seconds
        return \Velcoda\ApiAuth\Guards\VelcodaAuthGuard::decodeJWT($jwt);
    }

    protected function hasScope($jwt, $scope): bool
    {
        $scopes = self::getScopes($jwt);
        return in_array($scope, $scopes);
    }

    public static function castJwtToIdentity($jwt): IdentityBase {
        $jwt = self::decode($jwt);
        $data = $jwt->identity;
        return VelcodaAuthGuard::castToIdentity($data);
    }

    public static function getScopes($jwt): array {
        $jwt = self::decode($jwt);
        $data = $jwt->scopes;
        return $data;
    }

    public static function saveAccessToken($identity_id, $jwt) {
        Cache::put('at_' . $identity_id, $jwt);
    }

    public static function clearAccessToken($identity_id) {
        Cache::forget('at_' . $identity_id);
    }

    public static function getAccessToken($identity_id, $client_id = null, $client_secret = null) {
        if (!$client_id) {
            $client_id = env('IDENTITIES_AUTH_CLIENT_ID');
        }
        if (!$client_secret) {
            $client_secret = env('IDENTITIES_AUTH_CLIENT_SECRET');
        }
        $at = Cache::get('at_' . $identity_id);
        if (!self::isValid($at)) {
            $refresh_token = RefreshToken::get($identity_id);
            if ($refresh_token && $client_secret && $client_id) {
                $client = Identities::client();
                $resp = $client->refresh($refresh_token, $client_id, $client_secret);
                $token_set = $resp->body();
                RefreshToken::store($identity_id, $token_set->refresh_token);
                self::saveAccessToken($identity_id, $token_set->access_token);
                return $token_set->access_token;
            }
            self::clearAccessToken($identity_id);
            RefreshToken::forget($identity_id);
            throw new HTTP_UNAUTHORIZED('Access Token invalid');
        }
        return $at;
    }

    public static function refresh($identity_id, $refresh_token, $client_id, $client_secret) {
        $at = Cache::get('at_' . $identity_id);
        if (!self::isValid($at)) {
            return null;
        }
        return $at;
    }

    public static function isPartner(): bool
    {
        $user = self::getAuthenticatedUser();

        return (bool) $user?->isPartner();
    }

    public static function isRecordingStudio(): bool
    {
        $user = self::getAuthenticatedUser();

        return (bool) $user?->isRecordingStudio();
    }

    public static function isAdmin(): bool
    {
        $user = self::getAuthenticatedUser();

        return (bool) $user?->isAdmin();
    }

    public static function isDevopsAdmin(): bool
    {
        $user = self::getAuthenticatedUser();

        return (bool) $user?->isDevopsAdmin();
    }

    protected static function getAuthenticatedUser(): ?IdentityBase
    {
        return auth()->user();
    }
}
