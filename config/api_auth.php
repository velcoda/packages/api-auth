<?php

return [
    'certificates' => [
        'driver' => 's3',
        'key' => env('AWS_ACCESS_KEY_ID', 'xxxx'),
        'secret' => env('AWS_SECRET_ACCESS_KEY', 'xxxx'),
        'region' => env('DEFAULT_REGION', 'eu-central-1'),
        'bucket' => env('CERTIFICATE_KEYS_BUCKET', 'xxxx')
    ],
];