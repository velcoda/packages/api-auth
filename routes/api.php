<?php

use Illuminate\Support\Facades\Route;
use Velcoda\ApiAuth\Http\Controllers\ApiKeysController;
use Velcoda\ApiAuth\Http\Controllers\ApiKeyScopesController;
use Velcoda\ApiAuth\Http\Controllers\ServicesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth.api'], function () {
    Route::get('/services', [ServicesController::class, 'getAllServices']);
    Route::get('/meta', [ServicesController::class, 'show']);
    Route::get('/routes', [ServicesController::class, 'getAllRoutes']);


    Route::group(['prefix' => 'api-keys'], function () {
        Route::get('', [ApiKeysController::class, 'list']);
        Route::post('', [ApiKeysController::class, 'create']);
        Route::get('{key_id}', [ApiKeysController::class, 'show']);
        Route::patch('{key_id}', [ApiKeysController::class, 'update']);
        Route::delete('{key_id}', [ApiKeysController::class, 'delete']);
        Route::get('{key_id}/logs', [ApiKeysController::class, 'getLogs']);

        Route::group(['prefix' => '{key_id}/scopes'], function () {
            Route::get('', [ApiKeyScopesController::class, 'list']);
            Route::post('', [ApiKeyScopesController::class, 'add']);
            Route::delete('{scope_id}', [ApiKeyScopesController::class, 'remove']);
        });
    });

    Route::get('/scopes/{scope}', [ApiKeyScopesController::class, 'getKeysForScope']);
});
