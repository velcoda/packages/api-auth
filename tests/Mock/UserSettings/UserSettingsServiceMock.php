<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Mock\UserSettings;

use Mockery;
use Velcoda\Services\Http\Telebutler\UserSettings;

class UserSettingsServiceMock
{
    private array $settingsData = [
        'manage_users' => false
    ];

    private function __construct(array $settingsData = [])
    {
        $this->settingsData = array_merge(
            $this->settingsData,
            $settingsData
        );
    }

    /**
     * @param array{manage_users: bool} $settingsData
     */
    public static function mockSuccessForLoadSettingByIdentityId(
        array $settingsData = []
    ): void {
        $identitiesServiceMock = Mockery::mock('overload:' . UserSettings::class);

        $identitiesServiceMock->shouldReceive('client')
            ->andReturnUsing(static function () use ($settingsData) {
                return new UserSettingsServiceMock($settingsData);
            });
    }

    /**
     * @see \Velcoda\Services\Http\Telebutler\UserSettings::setApiKey()
     */
    public function setApiKey(): UserSettingsServiceMock
    {
        return $this;
    }

    /**
     * @see \Velcoda\Services\Http\Telebutler\UserSettings::loadSetting()
     */
    public function loadSetting($identity_id): BaseResponseMock
    {
        return new BaseResponseMock(
            $this->settingsData
        );
    }
}
