<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Mock\UserSettings;

use Velcoda\Services\Http\BaseResponse;

class BaseResponseMock extends BaseResponse
{
    public function __construct(private readonly array $data = [])
    {
    }

    public function body(): \stdClass
    {
        $responseData = [
            'data' => $this->data
        ];

        return json_decode(
            json_encode($responseData)
        );
    }
}
