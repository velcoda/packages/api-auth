<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Mock;

use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Services\DB\Velcoda\Identities;
use Mockery;

class IdentitiesServiceMock
{
    /**
     * Mock getById method and return identity
     * @see Identities::getById()
     */
    public static function mockAndReturn(IdentityBase $identityToReturn): void
    {
        $identitiesServiceMock = Mockery::mock('overload:' . Identities::class);

        $identitiesServiceMock->shouldReceive('getById')
            ->andReturnUsing(static function ($identityId) use ($identityToReturn) {
                return $identityToReturn;
            });
    }

    public static function mockAndReturnNull(): void
    {
        $identitiesServiceMock = Mockery::mock('overload:' . Identities::class);

        $identitiesServiceMock->shouldReceive('getById')
            ->andReturnUsing(static function ($identityId) {
                return null;
            });
    }
}
