<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Mock;

use Mockery;
use Velcoda\Services\DB\Velcoda\Customers;
use Velcoda\Services\Models\IntCustomer;

class CustomersServiceMock
{
    /**
     * @see Customers
     * @see IntCustomer
     */
    public static function mockAndReturn(IntCustomer $customer): void
    {
        $customersServiceMock = Mockery::mock('overload:' . Customers::class);

        $customersServiceMock->shouldReceive('getById')
            ->andReturn($customer);
    }

    public static function mockAndReturnNull(): void
    {
        $customersServiceMock = Mockery::mock('overload:' . Customers::class);

        $customersServiceMock->shouldReceive('getById')
            ->andReturn(null);
    }
}
