<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests;

use Illuminate\Config\Repository;
use Orchestra\Testbench\TestCase;

abstract class AbstractTestCase extends TestCase
{
    protected function defineEnvironment($app): void
    {
        /** @var Repository $config */
        $config = $app->get('config');

        $this->setVelcodaAsDefaultGuard($config);
        $this->setJwtKeys();
    }

    protected function setVelcodaAsDefaultGuard(Repository $config)
    {
        $config->set([
            'auth.defaults.guard' => 'velcoda',
            'auth.defaults.passwords' => null,

            'auth.guards.velcoda.driver' => 'velcoda',
            'auth.guards.velcoda.provider' => null
        ]);
    }

    protected function setJwtKeys(): void
    {
        $publicKey = file_get_contents(__DIR__ . '/jwt-keys/public.pem');
        $privateKey = file_get_contents(__DIR__ . '/jwt-keys/private.pem');

        putenv('JWT_PUBLIC_KEY=' . $publicKey);
        putenv('JWT_PRIVATE_KEY=' . $privateKey);
    }

    protected function unsetJwtKeys(): void
    {
        putenv('JWT_PUBLIC_KEY');
        putenv('JWT_PRIVATE_KEY');
    }
}
