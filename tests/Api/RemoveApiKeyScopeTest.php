<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class RemoveApiKeyScopeTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testRemoveApiKeyScope(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $scope = $this->apiKeyFactory->makeApiKeyScope('test.scope', $apiKey->id);
        $scope->save();

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->delete(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes/{$scope->id}"
            );

        $response->assertStatus(200);
    }

    public function testRemoveApiKeyScopeForNotExistingApiKey(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $scope = $this->apiKeyFactory->makeApiKeyScope('test.scope', $apiKey->id);
        $scope->save();

        $randomApiKeyId = fake()->uuid;

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->delete(
                "/v0/velcoda/api-keys/{$randomApiKeyId}/scopes/{$scope->id}"
            );

        $response->assertStatus(404);
    }

    public function testRemoveApiKeyScopeForNotExistingScope(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $scope = $this->apiKeyFactory->makeApiKeyScope('test.scope', $apiKey->id);
        $scope->save();

        $randomScopeId = fake()->uuid;

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->delete(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes/{$randomScopeId}"
            );

        $response->assertStatus(404);
    }

    public function testRemoveApiKeyScopeByNotAdminUser(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $scope = $this->apiKeyFactory->makeApiKeyScope('test.scope', $apiKey->id);
        $scope->save();

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->delete(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes/{$scope->id}"
            );

        $response->assertStatus(403);
    }

    public function testRemoveApiKeyScopeByPartner(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $scope = $this->apiKeyFactory->makeApiKeyScope('test.scope', $apiKey->id);
        $scope->save();

        $jwt = $this->jwtFactory->makeForPartner();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->delete(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes/{$scope->id}"
            );

        $response->assertStatus(403);
    }
}
