<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class ShowApiKeyTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testShowApiKey(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys/' . $apiKey->id);

        $response->assertStatus(200);

        $response->assertJsonPath(
            'data.id',
            $apiKey->id
        );

        $response->assertJsonPath(
            'data.key',
            $apiKey->key
        );

        $response->assertJsonPath(
            'data.name',
            $apiKey->name
        );
    }

    public function testShowNotExistingApiKey(): void
    {
        $randomApiKeyId = fake()->uuid;

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys/' . $randomApiKeyId);

        $response->assertStatus(404);
    }

    public function testShowApiKeyByNotAdminUser(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys/' . $apiKey->id);

        $response->assertStatus(403);
    }

    public function testShowApiKeyByPartner(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys/' . $apiKey->id);

        $response->assertStatus(403);
    }
}
