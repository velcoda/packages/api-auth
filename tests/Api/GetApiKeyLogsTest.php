<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class GetApiKeyLogsTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testGetApiKeyLogs(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $logsQuantity = 50;
        $this->apiKeyFactory->makeAndSaveManyApiKeyLogItems(
            $logsQuantity,
            $apiKey
        );

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                '/v0/velcoda/api-keys/' . $apiKey->id .'/logs'
            );

        $responseData = $response->json();

        $response->assertStatus(200);

        $this->assertSame(
            $logsQuantity,
            $responseData['meta']['total'],
            'Total log items must be ' . $logsQuantity
        );
    }

    public function testGetApiKeyLogsPaginationMetadata(): void
    {
        $page = 3;
        $perPageLimit = 10;

        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $logsQuantity = 50;
        $this->apiKeyFactory->makeAndSaveManyApiKeyLogItems(
            $logsQuantity,
            $apiKey
        );

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                "/v0/velcoda/api-keys/{$apiKey->id}/logs?page={$page}&limit={$perPageLimit}"
            );

        $responseData = $response->json();

        $response->assertStatus(200);

        $this->assertSame(
            $logsQuantity,
            $responseData['meta']['total'],
            'Total log items must be ' . $logsQuantity
        );

        $this->assertSame(
            $page,
            $responseData['meta']['current_page'],
            'Logs page must be ' . $page
        );

        $this->assertSame(
            $perPageLimit,
            $responseData['meta']['per_page'],
            'Logs per page limit must be ' . $perPageLimit
        );
    }

    public function testGetNotExistingApiKeyLogs(): void
    {
        $randomApiKeyId = fake()->uuid;

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                "/v0/velcoda/api-keys/{$randomApiKeyId}/logs"
            );

        $response->assertStatus(404);
    }

    public function testGetApiKeyLogsByNotAdminUser(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                '/v0/velcoda/api-keys/' . $apiKey->id .'/logs'
            );

        $response->assertStatus(403);
    }

    public function testGetApiKeyLogsByPartner(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->makeForPartner();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                '/v0/velcoda/api-keys/' . $apiKey->id .'/logs'
            );

        $response->assertStatus(403);
    }
}
