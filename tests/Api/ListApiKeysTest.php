<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class ListApiKeysTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testListAllApiKeys(): void
    {
        $apiKeysQuantity = 5;
        $this->apiKeyFactory->makeAndSaveManyApiKeys($apiKeysQuantity);

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys');

        $responseData = $response->json();

        $response->assertStatus(200);

        $this->assertCount(
            $apiKeysQuantity,
            $responseData['data'],
            "Response should contain {$apiKeysQuantity} API keys"
        );
    }

    public function testListApiKeysByScopeFilter(): void
    {
        $apiKeysWithoutScopesQuantity = 5;
        $this->apiKeyFactory->makeAndSaveManyApiKeys($apiKeysWithoutScopesQuantity);

        $scopeName = 'scope-test';
        $apiKeysWithScopeQuantity = 3;
        $this->apiKeyFactory->makeAndSaveManyApiKeys($apiKeysWithScopeQuantity, [$scopeName]);

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys?filter[scope]=' . $scopeName);

        $responseData = $response->json();

        $response->assertStatus(200);

        $this->assertCount(
            $apiKeysWithScopeQuantity,
            $responseData['data'],
            "Response should contain {$apiKeysWithScopeQuantity} API keys with scope"
        );
    }

    public function testListAllApiKeysByNotAdminUser(): void
    {
        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys');

        $response->assertStatus(403);
    }

    public function testListAllApiKeysByPartner(): void
    {
        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get('/v0/velcoda/api-keys');

        $response->assertStatus(403);
    }
}
