<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class AddApiKeyScopeTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testAddApiKeyScope(): void
    {
        $scopeName = 'test.add-scope';

        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes",
                [
                    'scope' => $scopeName
                ]
            );

        $response->assertStatus(200);

        $this->assertCount(
            1,
            $apiKey->scopes
        );
    }

    public function testAddNotExistingApiKeyScope(): void
    {
        $scopeName = 'test.add-scope';
        $randomApiKeyId = fake()->uuid;

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post(
                "/v0/velcoda/api-keys/{$randomApiKeyId}/scopes",
                [
                    'scope' => $scopeName
                ]
            );

        $response->assertStatus(404);
    }

    public function testAddApiKeyScopeByNotAdminUser(): void
    {
        $scopeName = 'test.add-scope';

        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes",
                [
                    'scope' => $scopeName
                ]
            );

        $response->assertStatus(403);
    }

    public function testAddApiScopeKeyByPartner(): void
    {
        $scopeName = 'test.add-scope';

        $apiKey = $this->apiKeyFactory->makeApiKey();
        $apiKey->save();

        $jwt = $this->jwtFactory->makeForPartner();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes",
                [
                    'scope' => $scopeName
                ]
            );

        $response->assertStatus(403);
    }
}
