<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class UpdateApiKeyTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    /**
     * Only active field should be updated
     */
    public function testUpdateApiKeyOnlyActiveField(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();

        $jwt = $this->jwtFactory->makeForAdmin();
        $newApiKeyData = [
            'name' => 'test.api-key.new-name',
            'key' => fake()->uuid,
            'active' => false
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->patch(
                '/v0/velcoda/api-keys/' . $apiKey->id,
                $newApiKeyData
            );

        $response->assertStatus(200);

        $response->assertJsonPath('data.name', $apiKey->name);
        $response->assertJsonPath('data.key', $apiKey->key);
        $response->assertJsonPath('data.active', $newApiKeyData['active']);
    }

    public function testUpdateNotExistingApiKey(): void
    {
        $randomApiKeyId = fake()->uuid;
        $newApiKeyData = [
            'active' => false
        ];

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->patch(
                '/v0/velcoda/api-keys/' .$randomApiKeyId,
                $newApiKeyData
            );

        $response->assertStatus(404);
    }

    public function testUpdateApiKeyByNotAdminUser(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $newApiKeyData = [
            'active' => false
        ];

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->patch(
                '/v0/velcoda/api-keys/' . $apiKey->id,
                $newApiKeyData
            );

        $response->assertStatus(403);
    }

    public function testUpdateApiKeyByPartner(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey();
        $newApiKeyData = [
            'active' => false
        ];

        $jwt = $this->jwtFactory->makeForPartner();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->patch(
                '/v0/velcoda/api-keys/' . $apiKey->id,
                $newApiKeyData
            );

        $response->assertStatus(403);
    }
}
