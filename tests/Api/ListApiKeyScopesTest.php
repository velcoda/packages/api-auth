<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class ListApiKeyScopesTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly ApiKeyFactory $apiKeyFactory;
    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testListApiKeyScopes(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey([
            'test.scope-1',
            'test.scope-2'
        ]);

        $apiKey->save();

        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes"
            );

        $responseData = $response->json();

        $response->assertStatus(200);

        $this->assertCount(
            2,
            $responseData,
            'Api key must have 2 scopes'
        );
    }

    public function testListNotExistingApiKeyScopes(): void
    {
        $randomApiKeyId = fake()->uuid;
        $jwt = $this->jwtFactory->makeForAdmin();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                "/v0/velcoda/api-keys/{$randomApiKeyId}/scopes"
            );

        $response->assertStatus(404);
    }

    public function testListApiKeyScopesByNotAdminUser(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey([
            'test.scope-1',
            'test.scope-2'
        ]);

        $apiKey->save();

        $jwt = $this->jwtFactory->make();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes"
            );

        $response->assertStatus(403);
    }

    public function testListApiKeyScopesByPartner(): void
    {
        $apiKey = $this->apiKeyFactory->makeApiKey([
            'test.scope-1',
            'test.scope-2'
        ]);

        $apiKey->save();

        $jwt = $this->jwtFactory->makeForPartner();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->get(
                "/v0/velcoda/api-keys/{$apiKey->id}/scopes"
            );

        $response->assertStatus(403);
    }
}
