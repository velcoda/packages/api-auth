<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

class CreateApiKeyTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private readonly JwtFactory $jwtFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testCreateApiKey(): void
    {
        $jwt = $this->jwtFactory->makeForAdmin();
        $requestData = [
            'name' => 'test.api-key',
            'token' => fake()->uuid,
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post('/v0/velcoda/api-keys', $requestData);

        $response->assertStatus(200);

        $response->assertJsonPath('data.name', $requestData['name']);
        $response->assertJsonPath('data.key', $requestData['token']);
    }

    public function testCreateApiKeyWithoutDotInName(): void
    {
        $jwt = $this->jwtFactory->makeForAdmin();
        $requestData = [
            'name' => 'incorrect-name-without-dot',
            'token' => fake()->uuid,
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->json('POST', '/v0/velcoda/api-keys', $requestData);

        $response->assertStatus(400);

        $response->assertJsonPath(
            'details',
            'Api Key must have one dot in name!'
        );
    }

    public function testCreateApiKeyWithoutManyDotsInName(): void
    {
        $jwt = $this->jwtFactory->makeForAdmin();
        $requestData = [
            'name' => 'incorrect-name.dot1.dot2',
            'token' => fake()->uuid,
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->json('POST', '/v0/velcoda/api-keys', $requestData);

        $response->assertStatus(400);

        $response->assertJsonPath(
            'details',
            'Api Key must have one dot in name!'
        );
    }

    public function testCreateApiKeyWithEmptyToken(): void
    {
        $jwt = $this->jwtFactory->makeForAdmin();
        $requestData = [
            'name' => 'test.api-key',
            'token' => '',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->json('POST', '/v0/velcoda/api-keys', $requestData);

        $response->assertStatus(422);

        $response->assertJsonPath(
            'errors.token.0',
            'The token field is required.'
        );
    }

    public function testCreateApiKeyByNotAdminUser(): void
    {
        $jwt = $this->jwtFactory->make();
        $requestData = [
            'name' => 'test.api-key',
            'token' => fake()->uuid,
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post('/v0/velcoda/api-keys', $requestData);

        $response->assertStatus(403);
    }

    public function testCreateApiKeyByPartner(): void
    {
        $jwt = $this->jwtFactory->makeForPartner();
        $requestData = [
            'name' => 'test.api-key',
            'token' => fake()->uuid,
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $jwt)
            ->post('/v0/velcoda/api-keys', $requestData);

        $response->assertStatus(403);
    }
}
