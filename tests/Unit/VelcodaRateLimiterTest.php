<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;

/**
 * @group rate-limiter
 */
class VelcodaRateLimiterTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private string $limiterName = 'velcoda';

    private ApiKeyFactory $apiKeyFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
    }

    public function testRateLimiterExisting(): void
    {
        $limiterCallback = RateLimiter::limiter(
            $this->limiterName
        );

        $this->assertNotNull($limiterCallback);
    }

    /**
     * Default settings is not related to the API Key scope
     */
    public function testRemainingAttemptsForDefaultSettings(): void
    {
        $defaultLimiterSettings = $this->getDefaultLimiterSettings();
        $limiterKeyByRequestIP = 'velcoda:127.0.0.1';
        $requestAttempts = 5;

        for ($i = 0; $i < $requestAttempts; $i++) {
            RateLimiter::hit($limiterKeyByRequestIP);
        }

        $this->assertSame(
            $requestAttempts,
            RateLimiter::attempts($limiterKeyByRequestIP)
        );

        $remainingRequestAttempts = RateLimiter::remaining($limiterKeyByRequestIP, $defaultLimiterSettings->maxAttempts);

        $this->assertSame(
            $defaultLimiterSettings->maxAttempts - $requestAttempts,
            $remainingRequestAttempts
        );
    }

    private function getDefaultLimiterSettings(): Limit
    {
        $limiterCallback = RateLimiter::limiter(
            $this->limiterName
        );

        $request = new Request();
        $request->server->add([
            'REMOTE_ADDR' => '127.0.0.1'
        ]);

        return $limiterCallback($request)[0];
    }

    public function testSettingsForApiKeyScope(): void
    {
        $scopeName = 'scope-test';
        $scopeRateLimit = 200;

        $apiKey = $this->apiKeyFactory->makeApiKeyForScopeWithRateLimit($scopeName, $scopeRateLimit);
        $apiKey->save();

        $apiKeyLimiterSettings = $this->getLimiterSettingsForApiKeyScope($apiKey, $scopeName);

        $this->assertSame(
            $scopeRateLimit,
            $apiKeyLimiterSettings->maxAttempts
        );
    }

    public function testRemainingAttemptsForApiKeyScope(): void
    {
        $scopeName = 'scope-test';
        $scopeRateLimit = 200;

        $apiKey = $this->apiKeyFactory->makeApiKeyForScopeWithRateLimit($scopeName, $scopeRateLimit);
        $apiKey->save();

        $apiKeyLimiterSettings = $this->getLimiterSettingsForApiKeyScope($apiKey, $scopeName);

        $limiterKeyByRequestIP = 'velcoda:' . $apiKey->id;
        $requestAttempts = 5;

        for ($i = 0; $i < $requestAttempts; $i++) {
            RateLimiter::hit($limiterKeyByRequestIP);
        }

        $this->assertSame(
            $requestAttempts,
            RateLimiter::attempts($limiterKeyByRequestIP)
        );

        $remainingRequestAttempts = RateLimiter::remaining($limiterKeyByRequestIP, $apiKeyLimiterSettings->maxAttempts);

        $this->assertSame(
            $apiKeyLimiterSettings->maxAttempts - $requestAttempts,
            $remainingRequestAttempts
        );
    }

    private function getLimiterSettingsForApiKeyScope(ApiKey $apiKey, string $scopeName): Limit
    {
        $limiterCallback = RateLimiter::limiter(
            $this->limiterName
        );

        $request = Request::create('v0/velcoda/services');

        $request->setRouteResolver(function () use ($scopeName) {
            $middleware = 'auth.api-key:' . $scopeName;

            return Route::middleware($middleware)
                ->get('/mock-route-with-middleware', static function() {
                    return new Response();
                });
        });

        $request->server->add([
            'REMOTE_ADDR' => '127.0.0.1'
        ]);

        $request->headers->set(
            AuthorizeApiKey::AUTH_HEADER,
            $apiKey->key
        );

        return $limiterCallback($request)[0];
    }
}
