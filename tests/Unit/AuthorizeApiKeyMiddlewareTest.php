<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\ApiKeyFactory;
use Velcoda\Exceptions\Exceptions\HTTP_FORBIDDEN;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

/**
 * @group api-key
 */
class AuthorizeApiKeyMiddlewareTest extends AbstractTestCase
{
    use WithWorkbench;
    use RefreshDatabase;

    private AuthorizeApiKey $middleware;
    private ApiKeyFactory $apiKeyFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->middleware = $this->app->make(AuthorizeApiKey::class);
        $this->apiKeyFactory = $this->app->make(ApiKeyFactory::class);
    }

    public function testEmptyApiKey(): void
    {
        $request = new Request();

        $this->expectException(HTTP_UNAUTHORIZED::class);

        $this->middleware->handle($request, function () {
            // mock next closure
        });
    }

    public function testInvalidApiKey(): void
    {
        $request = new Request();
        $request->headers->set(
            AuthorizeApiKey::AUTH_HEADER,
            'invalid_key'
        );

        $request->server->add([
            'REMOTE_ADDR' => '127.0.0.1'
        ]);

        $this->expectException(HTTP_UNAUTHORIZED::class);

        $this->middleware->handle($request, function () {
            // mock next closure
        });
    }

    public function testApiKeyWithValidScopes(): void
    {
        $scopes = ['scope.needed'];

        $apiKey = $this->apiKeyFactory->makeApiKey($scopes);
        $apiKey->save();

        $request = new Request();
        $request->headers->set(
            AuthorizeApiKey::AUTH_HEADER,
            $apiKey->key
        );

        $request->server->add([
            'REMOTE_ADDR' => '127.0.0.1'
        ]);

        $middlewareCallbackIsCalled = false;

        $this->middleware->handle($request, function () use (&$middlewareCallbackIsCalled) {
            $middlewareCallbackIsCalled = true;
        }, ...$scopes);

        $this->assertTrue($middlewareCallbackIsCalled);
    }

    public function testApiKeyWithInvalidScopes(): void
    {
        $scopes = ['scope.needed'];
        $scopeForApiKey = ['scope.invalid'];

        $apiKey = $this->apiKeyFactory->makeApiKey($scopeForApiKey);
        $apiKey->save();

        $request = new Request();
        $request->headers->set(
            AuthorizeApiKey::AUTH_HEADER,
            $apiKey->key
        );

        $request->server->add([
            'REMOTE_ADDR' => '127.0.0.1'
        ]);

        $this->expectException(HTTP_FORBIDDEN::class);

        $this->middleware->handle($request, function ($requestArg) use ($request) {
            // mock next closure
        }, ...$scopes);
    }
}
