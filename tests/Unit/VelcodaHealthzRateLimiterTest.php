<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\AbstractTestCase;

/**
 * @group rate-limiter
 */
class VelcodaHealthzRateLimiterTest extends AbstractTestCase
{
    use WithWorkbench;

    private string $limiterName = 'velcoda-healthz';

    public function testRateLimiterExisting(): void
    {
        $limiterCallback = RateLimiter::limiter(
            $this->limiterName
        );

        $this->assertNotNull($limiterCallback);
    }

    public function testRemainingAttempts(): void
    {
        $limiterSettings = $this->getLimiterSettings();
        $limiterKeyByRequestIP = 'velcoda-healthz:127.0.0.1';
        $requestAttempts = 5;

        for ($i = 0; $i < $requestAttempts; $i++) {
            RateLimiter::hit($limiterKeyByRequestIP);
        }

        $this->assertSame(
            $requestAttempts,
            RateLimiter::attempts($limiterKeyByRequestIP)
        );

        $remainingRequestAttempts = RateLimiter::remaining($limiterKeyByRequestIP, $limiterSettings->maxAttempts);

        $this->assertSame(
            $limiterSettings->maxAttempts - $requestAttempts,
            $remainingRequestAttempts
        );
    }

    private function getLimiterSettings(): Limit
    {
        $limiterCallback = RateLimiter::limiter(
            $this->limiterName
        );

        $request = new Request();
        $request->server->add([
            'REMOTE_ADDR' => '127.0.0.1'
        ]);

        return $limiterCallback($request);
    }
}
