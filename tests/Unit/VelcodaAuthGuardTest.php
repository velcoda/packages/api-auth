<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Http\Request;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Guards\VelcodaAuthGuard;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

/**
 * @group guard
 */
class VelcodaAuthGuardTest extends AbstractTestCase
{
    use WithWorkbench;

    private JwtFactory $jwtFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testGuestUserWithoutBearerHeader(): void
    {
        $guard = new VelcodaAuthGuard(
            $this->app
        );

        $this->assertFalse(
            $guard->check()
        );

        $this->assertTrue(
            $guard->guest()
        );

        $this->assertFalse(
            $guard->hasUser()
        );
    }

    public function testMalformedToken(): void
    {
        $request = new Request();
        $request->headers->set('Authorization', 'Bearer invalid_token');
        $this->app->request = $request;

        $guard = new VelcodaAuthGuard(
            $this->app
        );

        $this->expectException(
            HTTP_UNAUTHORIZED::class
        );

        $this->expectExceptionMessage('Access Token malformed');

        $guard->guest();
    }

    public function testValidToken(): void
    {
        $jwt = $this->jwtFactory->make();

        $request = new Request();
        $request->headers->set('Authorization', 'Bearer ' . $jwt);
        $this->app->request = $request;

        $guard = new VelcodaAuthGuard(
            $this->app
        );

        $this->assertTrue(
            $guard->check()
        );

        $this->assertFalse(
            $guard->guest()
        );

        $this->assertTrue(
            $guard->hasUser()
        );

        $this->assertInstanceOf(
            IdentityBase::class,
            $guard->user()
        );

        $this->assertIsString(
            $guard->id()
        );
    }
}
