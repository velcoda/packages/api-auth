<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Support\Facades\Auth;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Tests\Factory\CustomerFactory;
use Velcoda\ApiAuth\Tests\Mock\CustomersServiceMock;
use Velcoda\ApiAuth\Tests\Mock\IdentitiesServiceMock;
use Velcoda\ApiAuth\Tests\Mock\UserSettings\UserSettingsServiceMock;
use Velcoda\ApiAuth\Helpers\CommonAuthorizationRules;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\IdentityFactory;

/**
 * @group auth
 */
class CommonAuthorizationRulesTest extends AbstractTestCase
{
    use WithWorkbench;

    private readonly IdentityFactory $identityFactory;
    private readonly CustomerFactory $customerFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->identityFactory = $this->app->make(IdentityFactory::class);
        $this->customerFactory = $this->app->make(CustomerFactory::class);
    }

    public function test_fail_isMe_for_guest_user(): void
    {
        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isMe($randomIdentityId)
        );
    }

    public function test_fail_isMe_for_auth_user_but_other_identity(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity();
        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isMe($randomIdentityId)
        );
    }

    public function test_success_isMe_for_auth_user(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity();
        Auth::setUser($authIdentity);

        $trait = $this->getTraitObject();

        $this->assertTrue(
            $trait->isMe($authIdentity->id)
        );
    }

    public function test_fail_canManageCoworker_for_guest_user(): void
    {
        $coworkerIdentity = $this->identityFactory->makeIdentity();

        UserSettingsServiceMock::mockSuccessForLoadSettingByIdentityId([
            'manage_users' => true
        ]);

        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->canManageCoworker($coworkerIdentity)
        );
    }

    public function test_fail_canManageCoworker_between_different_customers(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => fake()->uuid
        ]);

        Auth::setUser($authIdentity);

        UserSettingsServiceMock::mockSuccessForLoadSettingByIdentityId([
            'manage_users' => true
        ]);

        $coworkerIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => fake()->uuid
        ]);

        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->canManageCoworker($coworkerIdentity)
        );
    }

    public function test_fail_canManageCoworker_for_same_customer_but_without_manages_user_settings(): void
    {
        $customerId = fake()->uuid;

        $authIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => $customerId
        ]);

        Auth::setUser($authIdentity);

        UserSettingsServiceMock::mockSuccessForLoadSettingByIdentityId([
            'manage_users' => false
        ]);

        $coworkerIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => $customerId
        ]);

        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->canManageCoworker($coworkerIdentity)
        );
    }

    public function test_success_canManageCoworker_for_same_customer_with_manage_users_settings(): void
    {
        $customerId = fake()->uuid;

        $authIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => $customerId
        ]);

        Auth::setUser($authIdentity);

        UserSettingsServiceMock::mockSuccessForLoadSettingByIdentityId([
            'manage_users' => true
        ]);

        $coworkerIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => $customerId
        ]);

        $trait = $this->getTraitObject();

        $this->assertTrue(
            $trait->canManageCoworker($coworkerIdentity)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomerByIdentityId_for_guest_user(): void
    {
        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomerByIdentityId_for_not_admin_and_not_partner(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'is_admin' => false,
            'is_partner' => false,
        ]);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_success_isAdminOrAgentOfCustomerByIdentityId_for_admin(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'is_admin' => true
        ]);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertTrue(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomerByIdentityId_for_not_existing_identity(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'is_partner' => true
        ]);

        Auth::setUser($authIdentity);

        IdentitiesServiceMock::mockAndReturnNull();

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomerByIdentityId_for_not_existing_customer(): void
    {
        $identity = $this->identityFactory->makeIdentity();
        IdentitiesServiceMock::mockAndReturn($identity);
        CustomersServiceMock::mockAndReturnNull();

        $authIdentity = $this->identityFactory->makeIdentity([
            'is_partner' => true
        ]);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomerByIdentityId_for_not_customer_agent(): void
    {
        $identity = $this->identityFactory->makeIdentity();
        IdentitiesServiceMock::mockAndReturn($identity);

        $customerWithoutAgent = $this->customerFactory->makeCustomer();
        CustomersServiceMock::mockAndReturn($customerWithoutAgent);

        $authIdentity = $this->identityFactory->makeIdentity([
            'is_partner' => true
        ]);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_success_isAdminOrAgentOfCustomerByIdentityId_for_customer_agent(): void
    {
        $identity = $this->identityFactory->makeIdentity();
        IdentitiesServiceMock::mockAndReturn($identity);

        $agentCustomerId = fake()->uuid;
        $authIdentity = $this->identityFactory->makeIdentity([
            'customer_id' => $agentCustomerId,
            'is_partner' => true,
        ]);

        $customerWithAgent = $this->customerFactory->makeCustomer($agentCustomerId);

        CustomersServiceMock::mockAndReturn($customerWithAgent);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertTrue(
            $trait->isAdminOrAgentOfCustomerByIdentityId($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomer_for_guest_user(): void
    {
        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomer($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomer_for_not_admin_and_not_partner(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'is_admin' => false,
            'is_partner' => false,
        ]);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomer($randomIdentityId)
        );
    }

    public function test_success_isAdminOrAgentOfCustomer_for_admin(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'is_admin' => true
        ]);

        Auth::setUser($authIdentity);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertTrue(
            $trait->isAdminOrAgentOfCustomer($randomIdentityId)
        );
    }

    public function test_success_isAdminOrAgentOfCustomer_for_partner_for_not_existing_customer(): void
    {
        $authIdentity = $this->identityFactory->makeIdentity([
            'is_partner' => true
        ]);

        Auth::setUser($authIdentity);

        CustomersServiceMock::mockAndReturnNull();

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomer($randomIdentityId)
        );
    }

    public function test_fail_isAdminOrAgentOfCustomer_for_customer_without_agent(): void
    {
        $agentCustomerId = fake()->uuid;

        $authIdentity = $this->identityFactory->makeIdentity([
            'is_partner' => true,
            'customer_id' => $agentCustomerId
        ]);

        Auth::setUser($authIdentity);

        $customerWithoutAgent = $this->customerFactory->makeCustomer();
        CustomersServiceMock::mockAndReturn($customerWithoutAgent);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertFalse(
            $trait->isAdminOrAgentOfCustomer($randomIdentityId)
        );
    }

    public function test_success_isAdminOrAgentOfCustomer_for_partner(): void
    {
        $agentCustomerId = fake()->uuid;

        $authIdentity = $this->identityFactory->makeIdentity([
            'is_partner' => true,
            'customer_id' => $agentCustomerId
        ]);

        Auth::setUser($authIdentity);

        $customerWithAgent = $this->customerFactory->makeCustomer($agentCustomerId);
        CustomersServiceMock::mockAndReturn($customerWithAgent);

        $randomIdentityId = fake()->uuid;
        $trait = $this->getTraitObject();

        $this->assertTrue(
            $trait->isAdminOrAgentOfCustomer($randomIdentityId)
        );
    }

    private function getTraitObject(): object
    {
        return new class
        {
            use CommonAuthorizationRules;
        };
    }
}
