<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Http\Request;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Http\Middleware\AuthenticateApiRequests;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

/**
 * @group auth-guard
 */
final class ApiAuthMiddlewareTest extends AbstractTestCase
{
    use WithWorkbench;

    private AuthenticateApiRequests $middleware;
    private JwtFactory $jwtFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->middleware = $this->app->make(AuthenticateApiRequests::class);
        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testWithoutBearerToken(): void
    {
        $request = new Request();

        $this->expectException(HTTP_UNAUTHORIZED::class);

        $this->middleware->handle($request, function () {
            // mock next closure
        });
    }

    public function testInvalidBearerToken(): void
    {
        $request = new Request();
        $request->headers->set('Authorization', 'Bearer invalid_token');
        $this->app->request = $request;

        $this->expectException(HTTP_UNAUTHORIZED::class);

        $this->middleware->handle($request, function () {
            // mock next closure
        });
    }

    public function testBearerTokenInvalidScope(): void
    {
        $neededScope = 'needed-scope';
        $tokenScopes = ['token-scope'];

        $jwt = $this->jwtFactory->make($tokenScopes);

        $request = new Request();
        $request->headers->set('Authorization', 'Bearer ' . $jwt);
        $this->app->request = $request;

        $this->expectException(HTTP_UNAUTHORIZED::class);
        $this->expectExceptionMessage('Scopes do not allow this action.');

        $this->middleware->handle($request, function ($requestArg) use ($request) {
            $this->assertSame($request, $requestArg);
        }, $neededScope);
    }

    public function testBearerTokenValidScope(): void
    {
        $neededScope = 'needed-scope';
        $tokenScopes = ['needed-scope'];

        $jwt = $this->jwtFactory->make($tokenScopes);

        $request = new Request();
        $request->headers->set('Authorization', 'Bearer ' . $jwt);
        $this->app->request = $request;

        $this->middleware->handle($request, function ($closureRequestArg) use ($request) {
            $this->assertSame($request, $closureRequestArg);
        }, $neededScope);
    }

    public function testBearerTokenAllScopes(): void
    {
        $tokenScopes = ['*'];

        $jwt = $this->jwtFactory->make($tokenScopes);

        $request = new Request();
        $request->headers->set('Authorization', 'Bearer ' . $jwt);
        $this->app->request = $request;

        $this->middleware->handle($request, function ($closureRequestArg) use ($request) {
            $this->assertSame($request, $closureRequestArg);
        });
    }
}
