<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Http\Services\JWT\JwtTokenService;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;

/**
 * @group jwt
 */
class JwtTokenServiceTest extends AbstractTestCase
{
    use WithWorkbench;

    private JwtFactory $jwtFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testJwtTokenPayload(): void
    {
        $scopes = ['view-settings', 'update-settings'];

        $jwt = $this->jwtFactory->make($scopes);
        $jwtPayload =  $this->jwtFactory->getPayloadFromJwtToken($jwt);

        $jwtService = new JwtTokenService($jwt);

        $this->assertSame(
            $jwtService->scopes(),
            $scopes
        );

        $this->assertSame(
            $jwtService->jwt_id(),
            $jwtPayload['jti']
        );

        $this->assertSame(
            $jwtService->audience(),
            $jwtPayload['aud']
        );

        $this->assertSame(
            $jwtService->sub(),
            $jwtPayload['sub']
        );

        $this->assertSame(
            $jwtService->exp(),
            $jwtPayload['exp']
        );

        $this->assertSame(
            $jwtService->iat(),
            $jwtPayload['iat']
        );

        $this->assertSame(
            $jwtService->nbf(),
            $jwtPayload['nbf']
        );
    }

    public function testJwtTokenIdentityPayload(): void
    {
        $jwt = $this->jwtFactory->make();
        $jwtPayload =  $this->jwtFactory->getPayloadFromJwtToken($jwt);
        $identityPayload = $jwtPayload['identity'];

        $jwtService = new JwtTokenService($jwt);
        $identity = $jwtService->getIdentity();

        $this->assertSame(
            $identityPayload['id'],
            $identity->id,
            'id is not the same'
        );

        $this->assertSame(
            $identityPayload['customer_id'],
            $identity->customer_id,
            'customer_id is not the same'
        );

        $this->assertSame(
            $identityPayload['email'],
            $identity->email,
            'email is not the same'
        );

        $this->assertSame(
            $identityPayload['title_before'],
            $identity->title_before,
            'title_before is not the same'
        );

        $this->assertSame(
            $identityPayload['first_name'],
            $identity->first_name,
            'first_name is not the same'
        );

        $this->assertSame(
            $identityPayload['middle_names'],
            $identity->middle_names,
            'middle_names is not the same'
        );

        $this->assertSame(
            $identityPayload['last_name'],
            $identity->last_name,
            'last_name is not the same'
        );

        $this->assertSame(
            $identityPayload['title_after'],
            $identity->title_after,
            'title_after is not the same'
        );

        $this->assertSame(
            $identityPayload['gender'],
            $identity->gender,
            'gender is not the same'
        );

        $this->assertSame(
            $identityPayload['email_verified_at'],
            $identity->email_verified_at,
            'email_verified_at is not the same'
        );
    }

    public function testExpiredToken(): void
    {
        $jwt = $this->jwtFactory->makeExpiredToken();

        $this->expectException(HTTP_UNAUTHORIZED::class);
        $this->expectExceptionMessage('Access Token Expired');

        $jwtService = new JwtTokenService($jwt);
    }

    public function testInvalidSignatureToken(): void
    {
        $jwt = $this->jwtFactory->makeInvalidSignatureToken();

        $this->expectException(HTTP_UNAUTHORIZED::class);
        $this->expectExceptionMessage('Invalid Access Token Signature');

        $jwtService = new JwtTokenService($jwt);
    }

    public function testNotJwtToken(): void
    {
        $jwt = 'not-jwt-token';

        $this->expectException(HTTP_UNAUTHORIZED::class);
        $this->expectExceptionMessage('Access Token malformed');

        $jwtService = new JwtTokenService($jwt);
    }

    public function testEmptyToken(): void
    {
        $jwt = '';

        $this->expectException(HTTP_UNAUTHORIZED::class);
        $this->expectExceptionMessage('');

        $jwtService = new JwtTokenService($jwt);
    }
}
