<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Http\Services\JWT\JwtTokenService;
use Velcoda\ApiAuth\Tests\AbstractTestCase;
use Velcoda\ApiAuth\Tests\Factory\JwtFactory;

/**
 * @group jwt
 */
class JwtTokenServiceNecessaryClaimsTest extends AbstractTestCase
{
    use WithWorkbench;

    private JwtFactory $jwtFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->jwtFactory = $this->app->make(JwtFactory::class);
    }

    public function testJwtWithoutSubClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$sub');

        $jwtService->sub();
    }

    public function testJwtWithoutJwtIdClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$jti');

        $jwtService->jwt_id();
    }

    public function testJwtWithoutAudienceClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$aud');

        $jwtService->audience();
    }

    public function testJwtWithoutIatClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$iat');

        $jwtService->iat();
    }

    public function testJwtWithoutNbfClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$nbf');

        $jwtService->nbf();
    }

    public function testJwtWithoutExpClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$exp');

        $jwtService->exp();
    }

    public function testJwtWithoutScopesClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$scopes');

        $jwtService->scopes();
    }

    public function testJwtWithoutIdentityClaim(): void
    {
        $jwtPayload = [];

        $jwt = $this->jwtFactory->makeFromCustomPayload($jwtPayload);
        $jwtService = new JwtTokenService($jwt);

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('Undefined property: stdClass::$identity');

        $jwtService->getIdentity();
    }
}
