<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Unit;

use Illuminate\Support\Facades\Cache;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Velcoda\ApiAuth\Http\Services\JWT\CertificateService;
use Velcoda\ApiAuth\Tests\AbstractTestCase;

/**
 * @group certificate-service
 */
class CertificateServiceTest extends AbstractTestCase
{
    use WithWorkbench;

    public function testCertificatesCache(): void
    {
        $this->assertTrue(
            CertificateService::loadKeys()
        );

        $this->assertTrue(
           Cache::has('jwt-keys')
        );
    }

    public function testCertificatesLoadingWithEmptyEnvironmentConfiguration(): void
    {
        $this->unsetJwtKeys();

        $this->expectException(
            \RuntimeException::class
        );

        $this->expectExceptionMessage(
            'Set valid JWT public key in environment JWT_PUBLIC_KEY variable.'
        );

        $this->assertTrue(
            CertificateService::loadKeys()
        );
    }
}
