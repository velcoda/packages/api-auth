<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Factory;

use Illuminate\Support\Str;
use Velcoda\ApiAuth\Models\ApiKey;
use Velcoda\ApiAuth\Models\ApiKeyAccessEvent;
use Velcoda\ApiAuth\Models\ApiKeyScope;

class ApiKeyFactory
{
    /**
     * @param string[] $scopes
     */
    public function makeApiKey(array $scopes = []): ApiKey
    {
        $apiKey = new ApiKey();
        $apiKey->id = fake()->uuid;
        $apiKey->name = 'test.api-key';
        $apiKey->key = fake()->uuid;
        $apiKey->active = true;
        $apiKey->save();

        $scopeModels = array_map(static function ($scope) use ($apiKey) {
            $scopeModel = new ApiKeyScope();
            $scopeModel->id = fake()->uuid;
            $scopeModel->api_key_id = $apiKey->id;
            $scopeModel->scope = $scope;

            return $scopeModel;
        }, $scopes);

        $apiKey->scopes()->saveMany(
            $scopeModels
        );

        return $apiKey;
    }

    public function makeApiKeyForScopeWithRateLimit(string $scopeName, int $rateLimit): ApiKey
    {
        $apiKey = new ApiKey();
        $apiKey->id = fake()->uuid;
        $apiKey->name = 'test.api-key';
        $apiKey->key = fake()->uuid;
        $apiKey->active = true;
        $apiKey->save();

        $scopeModel = new ApiKeyScope();
        $scopeModel->id = fake()->uuid;
        $scopeModel->api_key_id = $apiKey->id;
        $scopeModel->scope = $scopeName;
        $scopeModel->rate_limit = $rateLimit;

        $apiKey->scopes()->save($scopeModel);

        return $apiKey;
    }

    public function makeAndSaveManyApiKeys(int $apiKeysQuantity, array $scopes = []): void
    {
        for ($i = 0; $i < $apiKeysQuantity; $i++) {
            $apiKey = $this->makeApiKey($scopes);
            $apiKey->save();
        }
    }

    public function makeAndSaveManyApiKeyLogItems(int $logsQuantity, ApiKey $apiKey): void
    {
        for ($i = 0; $i < $logsQuantity; $i++) {
            $logItem = new ApiKeyAccessEvent();

            $logItem->id = fake()->uuid;
            $logItem->api_key_id = $apiKey->id;
            $logItem->api_key_name = $apiKey->name;
            $logItem->ip_address = '127.0.0.1';
            $logItem->url = 'http://test-url';
            $logItem->method = 'GET';
            $logItem->context = '';
            $logItem->save();

            $logItem->save();
        }
    }

    public function makeApiKeyScope(string $scopeName, string $apiKeyId): ApiKeyScope
    {
        $scope = new ApiKeyScope();
        $scope->id = fake()->uuid;
        $scope->api_key_id = $apiKeyId;
        $scope->scope = $scopeName;

        return $scope;
    }
}
