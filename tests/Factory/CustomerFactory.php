<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Factory;

use Velcoda\Services\Models\IntCustomer;

class CustomerFactory
{
    public function makeCustomer(?string $agentId = null): IntCustomer
    {
        $customer = new IntCustomer();
        $customer->id = fake()->uuid;
        $customer->agent_customer_id = $agentId;

        return $customer;
    }
}
