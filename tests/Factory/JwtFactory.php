<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Factory;

use Firebase\JWT\JWT;

class JwtFactory
{
    private string $privateKey;
    private string $alg = 'RS256';

    public function __construct()
    {
        $this->privateKey = env('JWT_PRIVATE_KEY');
    }

    public function make(array $scopes = []): string
    {
        $payload = [
            'identity' => [
                'id'                => fake()->uuid,
                'customer_id'       => fake()->uuid,
                'title_before'      => 'Mr.',
                'first_name'        => fake()->firstName,
                'middle_names'      => fake()->lastName,
                'last_name'         => fake()->lastName,
                'title_after'       => 'After.',
                'gender'            => fake()->randomLetter,
                'email'             => fake()->email,
                'email_verified_at' => null,
            ],
            'scopes' => $scopes,
            'jti'    => fake()->uuid,
            'aud'    => 'mock-service',
            'sub'    => fake()->uuid,
            'exp'    => fake()->dateTimeBetween('+1 day', '+10 day')->getTimestamp(),
            'iat'    => fake()->unixTime,
            'nbf'    => time(),
        ];

        return JWT::encode($payload, $this->privateKey, $this->alg);
    }

    public function getPayloadFromJwtToken(string $jwt): array
    {
        $tokenParts = explode('.', $jwt);

        $body = $tokenParts[1];

        return json_decode(JWT::urlsafeB64Decode($body), true);
    }

    public function makeExpiredToken(): string
    {
        $payload = [
            'identity' => [
                'id'                => fake()->uuid,
                'customer_id'       => fake()->uuid,
                'title_before'      => 'Mr.',
                'first_name'        => fake()->firstName,
                'middle_names'      => fake()->lastName,
                'last_name'         => fake()->lastName,
                'title_after'       => 'After.',
                'gender'            => fake()->randomLetter,
                'email'             => fake()->email,
                'email_verified_at' => null,
            ],
            'scopes' => [],
            'jti'    => fake()->uuid,
            'aud'    => 'mock-service',
            'sub'    => fake()->uuid,
            'exp'    => fake()->dateTimeBetween('-10 day', '-1 day')->getTimestamp(),
            'iat'    => fake()->unixTime,
            'nbf'    => time(),
        ];

        return JWT::encode($payload, $this->privateKey, $this->alg);
    }

    public function makeInvalidSignatureToken(): string
    {
        $payload = [
            'identity' => [
                'id'                => fake()->uuid,
                'customer_id'       => fake()->uuid,
                'title_before'      => 'Mr.',
                'first_name'        => fake()->firstName,
                'middle_names'      => fake()->lastName,
                'last_name'         => fake()->lastName,
                'title_after'       => 'After.',
                'gender'            => fake()->randomLetter,
                'email'             => fake()->email,
                'email_verified_at' => null,
            ],
            'scopes' => [],
            'jti'    => fake()->uuid,
            'aud'    => 'mock-service',
            'sub'    => fake()->uuid,
            'exp'    => fake()->dateTimeBetween('-10 day', '-1 day')->getTimestamp(),
            'iat'    => fake()->unixTime,
            'nbf'    => time(),
        ];

        $jwt = JWT::encode($payload, $this->privateKey, $this->alg);
        $jwt .= 'signature-salt';

        return $jwt;
    }

    public function makeFromCustomPayload(array $payload = []): string
    {
        return JWT::encode($payload, $this->privateKey, $this->alg);
    }

    public function makeForAdmin(array $scopes = []): string
    {
        $payload = [
            'identity' => [
                'id'                => fake()->uuid,
                'customer_id'       => fake()->uuid,
                'title_before'      => 'Mr.',
                'first_name'        => fake()->firstName,
                'middle_names'      => fake()->lastName,
                'last_name'         => fake()->lastName,
                'title_after'       => 'After.',
                'gender'            => fake()->randomLetter,
                'email'             => fake()->email,
                'email_verified_at' => null,
                'is_admin'          => true,
            ],
            'scopes' => $scopes,
            'jti'    => fake()->uuid,
            'aud'    => 'mock-service',
            'sub'    => fake()->uuid,
            'exp'    => fake()->dateTimeBetween('+1 day', '+10 day')->getTimestamp(),
            'iat'    => fake()->unixTime,
            'nbf'    => time(),
        ];

        return JWT::encode($payload, $this->privateKey, $this->alg);
    }

    public function makeForPartner(array $scopes = []): string
    {
        $payload = [
            'identity' => [
                'id' => fake()->uuid,
                'customer_id' => fake()->uuid,
                'title_before' => 'Mr.',
                'first_name' => fake()->firstName,
                'middle_names' => fake()->lastName,
                'last_name' => fake()->lastName,
                'title_after' => 'After.',
                'gender' => fake()->randomLetter,
                'email' => fake()->email,
                'email_verified_at' => null,
                'is_partner' => true
            ],
            'scopes' => $scopes,
            'jti' => fake()->uuid,
            'aud' => 'mock-service',
            'sub' => fake()->uuid,
            'exp' => fake()->dateTimeBetween('+1 day', '+10 day')->getTimestamp(),
            'iat' => fake()->unixTime,
            'nbf' => time()
        ];

        return JWT::encode(
            $payload,
            $this->privateKey,
            $this->alg
        );
    }
}
