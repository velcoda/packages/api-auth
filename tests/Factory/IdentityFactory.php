<?php

declare(strict_types=1);

namespace Velcoda\ApiAuth\Tests\Factory;

use Velcoda\ApiAuth\Models\IdentityBase;

class IdentityFactory
{
    public function makeIdentity(array $data = []): IdentityBase
    {
        return IdentityBase::fromArray([
            'id'                => fake()->uuid,
            'customer_id'       => fake()->uuid,
            'title_before'      => '',
            'first_name'        => 'Unit',
            'middle_names'      => '',
            'last_name'         => 'Test',
            'title_after'       => '',
            'gender'            => '',
            'email'             => '',
            'email_verified_at' => '',
            'created_at'        => '',
            'updated_at'        => '',
            'is_admin' => false,
            'is_partner' => false,
            ...$data
        ]);
    }
}
