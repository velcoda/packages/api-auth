FROM  registry.gitlab.com/velcoda/k8s/docker-base:php8.2

COPY --chown=nginx:nginx composer.* ./
COPY . /code
RUN composer install
